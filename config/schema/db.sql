/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  bryan
 * Created: Mar 26, 2017
 */

drop schema sw_calculator;
create schema sw_calculator;

use sw_calculator;

CREATE TABLE IF NOT EXISTS units_list (
    id int not null,
    awaken boolean not null default 0,
    element_id int not null,
    description varchar(255) not null default '' COMMENT 'AKA name',
    CONSTRAINT pk_units PRIMARY KEY (id, awaken, element_id)
);

CREATE TABLE IF NOT EXISTS units (
    id bigint not null primary key,
    element_id int null,
    hp int null default 0,
    atk int null default 0,
    def int null default 0,
    spd int null default 0,
    cri_rate int null default 0,
    cri_damage int null default 0,
    resistance int null default 0,
    accuracy int null default 0,
    awaken boolean default 0,
    units_list_id int null,
    unit_level int null,
    create_time datetime null,
    stars int null
);

CREATE TABLE IF NOT EXISTS runes (
    id bigint not null primary key,
    slot_number int not null,
    stars int not null,
    rune_set_id int not null,
    upgrade_curr int null,
    unit_id bigint null
);

CREATE TABLE IF NOT EXISTS rune_attributes (
    id int not null auto_increment primary key,
    rune_id bigint not null,
    delta int not null,
    identifier varchar(12) COMMENT 'Main, Prefix, Slot 1, Slot 2, Slot 3, Slot 4',
    attribute_id int not null,
    increase int not null,
    modified boolean null,
    boost int null
);

CREATE TABLE rune_sets (
    id int not null primary key,
    description varchar(15) not null COMMENT 'AKA name',
    requirement int null,
    increase_amount int null,
    attribute_id int null
);

CREATE TABLE attributes (
    id int not null primary key,
    description varchar(255) not null COMMENT 'AKA name'
) COMMENT = 'Atk, Spd def, etc';

CREATE TABLE elements (
    id int not null primary key,
    description varchar(12) not null COMMENT 'AKA name'
);

/** Foreign keys manipulation. */

ALTER TABLE units
ADD FOREIGN KEY fk_units_list (units_list_id, awaken, element_id) REFERENCES units_list (id, awaken, element_id) ON DELETE CASCADE ON UPDATE CASCADE,
ADD FOREIGN KEY fk_units_element (element_id) REFERENCES elements (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE units_list
ADD FOREIGN KEY fk_units_list_element (element_id) REFERENCES elements (id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE runes
ADD FOREIGN KEY fk_runes_unit (unit_id) REFERENCES units (id) ON DELETE SET NULL ON UPDATE SET NULL,
ADD FOREIGN KEY fk_runes_rune_set (rune_set_id) REFERENCES rune_sets (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE rune_attributes
ADD FOREIGN KEY fk_rune_attributes_rune (rune_id) REFERENCES runes (id) ON DELETE CASCADE ON UPDATE CASCADE,
ADD FOREIGN KEY fk_rune_attributes_attribute (attribute_id) REFERENCES attributes (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE rune_sets
ADD FOREIGN KEY fk_rune_sets_attribute (attribute_id) REFERENCES attributes (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE rune_attributes
ADD COLUMN label varchar(15) null default null AFTER identifier;

