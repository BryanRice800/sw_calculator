/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  bryan
 * Created: Mar 26, 2017
 */


use sw_calculator;
/** Data insertion */

insert into elements values
(0, 'None'),
(1, 'Water'),
(2, 'Fire'),
(3, 'Wind'),
(4, 'Light'),
(5, 'Dark');

insert into attributes values
(0, 'None'),
(1, 'HP'),
(2, 'HP_P'),
(3, 'ATK'),
(4, 'ATK_P'),
(5, 'DEF'),
(6, 'DEF_P'),
(7, 'UNKNOWN'),
(8, 'SPD'),
(9, 'Cri_Rate'),
(10, 'Cri_Damage'),
(11, 'Resistance'),
(12, 'Accuracy'),
(13, 'SPD_P');

insert into rune_sets values
(0, 'None', 0, 0, 0),
(1, 'Energy', 2, 15, 2),
(2, 'Guard', 2, 15, 7),
(3, 'Swift', 4, 25, 13),
(4, 'Blade', 2, 12, 9),
(5, 'Rage', 4, 40, 10),
(6, 'Focus', 2, 20, 12),
(7, 'Endure', 2, 20, 11),
(8, 'Fatal', 4, 35, 4),
(10, 'Despair', 4, 0, 0),
(11, 'Vampire', 4, 0, 0),
(13, 'Violent', 4, 0, 0),
(14, 'Nemesis', 2, 0, 0),
(15, 'Will', 2, 0, 0),
(16, 'Shield', 2, 0, 0),
(17, 'Revenge', 2, 0, 0),
(18, 'Destroy', 2, 0, 0),
(19, 'Fight', 2, 7, 4),
(20, 'Determination', 2, 7, 6),
(21, 'Enhance', 2, 7, 2),
(22, 'Accuracy', 2, 10, 12),
(23, 'Tolerance', 2, 10, 11);

-- insert into units_


insert into units_list values
('100',0,1,'Homunculus Water'),
('100',0,2,'Homunculus Fire'),
('100',0,3,'Homunculus Wind'),
('100',0,4,'Homunculus Light'),
('100',0,5,'Homunculus Dark'),
('100',1,1,'Awakened Water Homunculus'),
('100',1,2,'Awakened Fire Homunculus'),
('100',1,3,'Awakened Wind Homunculus'),
('100',1,4,'Awakened Light Homunculus'),
('100',1,5,'Awakened Dark Homunculus'),
('101',0,1,'Fairy'),
('101',0,2,'Fairy'),
('101',0,3,'Fairy'),
('101',0,4,'Fairy'),
('101',0,5,'Fairy'),
('101',1,1,'Elucia'),
('101',1,2,'Iselia'),
('101',1,3,'Aeilene'),
('101',1,4,'Neal'),
('101',1,5,'Sorin'),
('102',0,1,'Imp'),
('102',0,2,'Imp'),
('102',0,3,'Imp'),
('102',0,4,'Imp'),
('102',0,5,'Imp'),
('102',1,1,'Fynn'),
('102',1,2,'Cogma'),
('102',1,3,'Ralph'),
('102',1,4,'Taru'),
('102',1,5,'Garok'),
('103',0,1,'Pixie'),
('103',0,2,'Pixie'),
('103',0,3,'Pixie'),
('103',0,4,'Pixie'),
('103',0,5,'Pixie'),
('103',1,1,'Kacey'),
('103',1,2,'Tatu'),
('103',1,3,'Shannon'),
('103',1,4,'Cheryl'),
('103',1,5,'Camaryn'),
('104',0,1,'Yeti'),
('104',0,2,'Yeti'),
('104',0,3,'Yeti'),
('104',0,4,'Yeti'),
('104',0,5,'Yeti'),
('104',1,1,'Kunda'),
('104',1,2,'Tantra'),
('104',1,3,'Rakaja'),
('104',1,4,'Arkajan'),
('104',1,5,'Kumae'),
('105',0,1,'Harpy'),
('105',0,2,'Harpy'),
('105',0,3,'Harpy'),
('105',0,4,'Harpy'),
('105',0,5,'Harpy'),
('105',1,1,'Ramira'),
('105',1,2,'Lucasha'),
('105',1,3,'Prilea'),
('105',1,4,'Kabilla'),
('105',1,5,'Hellea'),
('106',0,1,'Hellhound'),
('106',0,2,'Hellhound'),
('106',0,3,'Hellhound'),
('106',0,4,'Hellhound'),
('106',0,5,'Hellhound'),
('106',1,1,'Tarq'),
('106',1,2,'Sieq'),
('106',1,3,'Gamir'),
('106',1,4,'Shamar'),
('106',1,5,'Shumar'),
('107',0,1,'Warbear'),
('107',0,2,'Warbear'),
('107',0,3,'Warbear'),
('107',0,4,'Warbear'),
('107',0,5,'Warbear'),
('107',1,1,'Dagora'),
('107',1,2,'Ursha'),
('107',1,3,'Ramagos'),
('107',1,4,'Lusha'),
('107',1,5,'Gorgo'),
('108',0,1,'Elemental'),
('108',0,2,'Elemental'),
('108',0,3,'Elemental'),
('108',0,4,'Elemental'),
('108',0,5,'Elemental'),
('108',1,1,'Daharenos'),
('108',1,2,'Bremis'),
('108',1,3,'Taharus'),
('108',1,4,'Priz'),
('108',1,5,'Camules'),
('109',0,1,'Garuda'),
('109',0,2,'Garuda'),
('109',0,3,'Garuda'),
('109',0,4,'Garuda'),
('109',0,5,'Garuda'),
('109',1,1,'Konamiya'),
('109',1,2,'Cahule'),
('109',1,3,'Lindermen'),
('109',1,4,'Teon'),
('109',1,5,'Rizak'),
('110',0,1,'Inugami'),
('110',0,2,'Inugami'),
('110',0,3,'Inugami'),
('110',0,4,'Inugami'),
('110',0,5,'Inugami'),
('110',1,1,'Icaru'),
('110',1,2,'Raoq'),
('110',1,3,'Ramahan'),
('110',1,4,'Belladeon'),
('110',1,5,'Kro'),
('111',0,1,'Salamander'),
('111',0,2,'Salamander'),
('111',0,3,'Salamander'),
('111',0,4,'Salamander'),
('111',0,5,'Salamander'),
('111',1,1,'Kaimann'),
('111',1,2,'Krakdon'),
('111',1,3,'Lukan'),
('111',1,4,'Sharman'),
('111',1,5,'Decamaron'),
('112',0,1,'Nine-tailed Fox'),
('112',0,2,'Nine-tailed Fox'),
('112',0,3,'Nine-tailed Fox'),
('112',0,4,'Nine-tailed Fox'),
('112',0,5,'Nine-tailed Fox'),
('112',1,1,'Soha'),
('112',1,2,'Shihwa'),
('112',1,3,'Arang'),
('112',1,4,'Chamie'),
('112',1,5,'Kamiya'),
('113',0,1,'Serpent'),
('113',0,2,'Serpent'),
('113',0,3,'Serpent'),
('113',0,4,'Serpent'),
('113',0,5,'Serpent'),
('113',1,1,'Shailoq'),
('113',1,2,'Fao'),
('113',1,3,'Ermeda'),
('113',1,4,'Elpuria'),
('113',1,5,'Mantura'),
('114',0,1,'Golem'),
('114',0,2,'Golem'),
('114',0,3,'Golem'),
('114',0,4,'Golem'),
('114',0,5,'Golem'),
('114',1,1,'Kuhn'),
('114',1,2,'Kugo'),
('114',1,3,'Ragion'),
('114',1,4,'Groggo'),
('114',1,5,'Maggi'),
('115',0,1,'Griffon'),
('115',0,2,'Griffon'),
('115',0,3,'Griffon'),
('115',0,4,'Griffon'),
('115',0,5,'Griffon'),
('115',1,1,'Kahn'),
('115',1,2,'Spectra'),
('115',1,3,'Bernard'),
('115',1,4,'Shamann'),
('115',1,5,'Varus'),
('116',0,1,'Undine'),
('116',0,2,'Undine'),
('116',0,3,'Undine'),
('116',0,4,'Undine'),
('116',0,5,'Undine'),
('116',1,1,'Mikene'),
('116',1,2,'Atenai'),
('116',1,3,'Delphoi'),
('116',1,4,'Icasha'),
('116',1,5,'Tilasha'),
('117',0,1,'Inferno'),
('117',0,2,'Inferno'),
('117',0,3,'Inferno'),
('117',0,4,'Inferno'),
('117',0,5,'Inferno'),
('117',1,1,'Purian'),
('117',1,2,'Tagaros'),
('117',1,3,'Anduril'),
('117',1,4,'Eludain'),
('117',1,5,'Drogan'),
('118',0,1,'Sylph'),
('118',0,2,'Sylph'),
('118',0,3,'Sylph'),
('118',0,4,'Sylph'),
('118',0,5,'Sylph'),
('118',1,1,'Tyron'),
('118',1,2,'Baretta'),
('118',1,3,'Shimitae'),
('118',1,4,'Eredas'),
('118',1,5,'Aschubel'),
('119',0,1,'Sylphid'),
('119',0,2,'Sylphid'),
('119',0,3,'Sylphid'),
('119',0,4,'Sylphid'),
('119',0,5,'Sylphid'),
('119',1,1,'Lumirecia'),
('119',1,2,'Fria'),
('119',1,3,'Acasis'),
('119',1,4,'Mihael'),
('119',1,5,'Icares'),
('120',0,1,'High Elemental'),
('120',0,2,'High Elemental'),
('120',0,3,'High Elemental'),
('120',0,4,'High Elemental'),
('120',0,5,'High Elemental'),
('120',1,1,'Ellena'),
('120',1,2,'Kahli'),
('120',1,3,'Moria'),
('120',1,4,'Shren'),
('120',1,5,'Jumaline'),
('121',0,1,'Harpu'),
('121',0,2,'Harpu'),
('121',0,3,'Harpu'),
('121',0,4,'Harpu'),
('121',0,5,'Harpu'),
('121',1,1,'Sisroo'),
('121',1,2,'Colleen'),
('121',1,3,'Seal'),
('121',1,4,'Sia'),
('121',1,5,'Seren'),
('122',0,1,'Slime'),
('122',0,2,'Slime'),
('122',0,3,'Slime'),
('122',0,4,'Slime'),
('122',0,5,'Slime'),
('123',0,1,'Forest Keeper'),
('123',0,2,'Forest Keeper'),
('123',0,3,'Forest Keeper'),
('123',0,4,'Forest Keeper'),
('123',0,5,'Forest Keeper'),
('124',0,1,'Mushroom'),
('124',0,2,'Mushroom'),
('124',0,3,'Mushroom'),
('124',0,4,'Mushroom'),
('124',0,5,'Mushroom'),
('125',0,1,'Maned Boar'),
('125',0,2,'Maned Boar'),
('125',0,3,'Maned Boar'),
('125',0,4,'Maned Boar'),
('125',0,5,'Maned Boar'),
('126',0,1,'Monster Flower'),
('126',0,2,'Monster Flower'),
('126',0,3,'Monster Flower'),
('126',0,4,'Monster Flower'),
('126',0,5,'Monster Flower'),
('127',0,1,'Ghost'),
('127',0,2,'Ghost'),
('127',0,3,'Ghost'),
('127',0,4,'Ghost'),
('127',0,5,'Ghost'),
('128',0,1,'Low Elemental'),
('128',0,2,'Low Elemental'),
('128',0,3,'Low Elemental'),
('128',0,4,'Low Elemental'),
('128',0,5,'Low Elemental'),
('128',1,1,'Tigresse'),
('128',1,2,'Lamor'),
('128',1,3,'Samour'),
('128',1,4,'Varis'),
('128',1,5,'Havana'),
('129',0,1,'Mimick'),
('129',0,2,'Mimick'),
('129',0,3,'Mimick'),
('129',0,4,'Mimick'),
('129',0,5,'Mimick'),
('130',0,1,'Horned Frog'),
('130',0,2,'Horned Frog'),
('130',0,3,'Horned Frog'),
('130',0,4,'Horned Frog'),
('130',0,5,'Horned Frog'),
('131',0,1,'Sandman'),
('131',0,2,'Sandman'),
('131',0,3,'Sandman'),
('131',0,4,'Sandman'),
('131',0,5,'Sandman'),
('132',0,1,'Howl'),
('132',0,2,'Howl'),
('132',0,3,'Howl'),
('132',0,4,'Howl'),
('132',0,5,'Howl'),
('132',1,1,'Lulu'),
('132',1,2,'Lala'),
('132',1,3,'Chichi'),
('132',1,4,'Shushu'),
('132',1,5,'Chacha'),
('133',0,1,'Succubus'),
('133',0,2,'Succubus'),
('133',0,3,'Succubus'),
('133',0,4,'Succubus'),
('133',0,5,'Succubus'),
('133',1,1,'Izaria'),
('133',1,2,'Akia'),
('133',1,3,'Selena'),
('133',1,4,'Aria'),
('133',1,5,'Isael'),
('134',0,1,'Joker'),
('134',0,2,'Joker'),
('134',0,3,'Joker'),
('134',0,4,'Joker'),
('134',0,5,'Joker'),
('134',1,1,'Sian'),
('134',1,2,'Jojo'),
('134',1,3,'Lushen'),
('134',1,4,'Figaro'),
('134',1,5,'Liebli'),
('135',0,1,'Ninja'),
('135',0,2,'Ninja'),
('135',0,3,'Ninja'),
('135',0,4,'Ninja'),
('135',0,5,'Ninja'),
('135',1,1,'Susano'),
('135',1,2,'Garo'),
('135',1,3,'Orochi'),
('135',1,4,'Gin'),
('135',1,5,'Han'),
('136',0,1,'Surprise Box'),
('136',0,2,'Surprise Box'),
('136',0,3,'Surprise Box'),
('136',0,4,'Surprise Box'),
('136',0,5,'Surprise Box'),
('137',0,1,'Bearman'),
('137',0,2,'Bearman'),
('137',0,3,'Bearman'),
('137',0,4,'Bearman'),
('137',0,5,'Bearman'),
('137',1,1,'Gruda'),
('137',1,2,'Kungen'),
('137',1,3,'Dagorr'),
('137',1,4,'Ahman'),
('137',1,5,'Haken'),
('138',0,1,'Valkyrja'),
('138',0,2,'Valkyrja'),
('138',0,3,'Valkyrja'),
('138',0,4,'Valkyrja'),
('138',0,5,'Valkyrja'),
('138',1,1,'Camilla'),
('138',1,2,'Vanessa'),
('138',1,3,'Katarina'),
('138',1,4,'Akroma'),
('138',1,5,'Trinity'),
('139',0,1,'Pierret'),
('139',0,2,'Pierret'),
('139',0,3,'Pierret'),
('139',0,4,'Pierret'),
('139',0,5,'Pierret'),
('139',1,1,'Julie'),
('139',1,2,'Clara'),
('139',1,3,'Sophia'),
('139',1,4,'Eva'),
('139',1,5,'Luna'),
('140',0,1,'Werewolf'),
('140',0,2,'Werewolf'),
('140',0,3,'Werewolf'),
('140',0,4,'Werewolf'),
('140',0,5,'Werewolf'),
('140',1,1,'Vigor'),
('140',1,2,'Garoche'),
('140',1,3,'Shakan'),
('140',1,4,'Eshir'),
('140',1,5,'Jultan'),
('141',0,1,'Phantom Thief'),
('141',0,2,'Phantom Thief'),
('141',0,3,'Phantom Thief'),
('141',0,4,'Phantom Thief'),
('141',0,5,'Phantom Thief'),
('141',1,1,'Luer'),
('141',1,2,'Jean'),
('141',1,3,'Julien'),
('141',1,4,'Louis'),
('141',1,5,'Guillaume'),
('142',0,1,'Angelmon'),
('142',0,2,'Angelmon'),
('142',0,3,'Angelmon'),
('142',0,4,'Angelmon'),
('142',0,5,'Angelmon'),
('142',1,1,'Blue Angelmon'),
('142',1,2,'Red Angelmon'),
('142',1,3,'Gold Angelmon'),
('142',1,4,'White Angelmon'),
('142',1,5,'Dark Angelmon'),
('144',0,1,'Dragon'),
('144',0,2,'Dragon'),
('144',0,3,'Dragon'),
('144',0,4,'Dragon'),
('144',0,5,'Dragon'),
('144',1,1,'Verad'),
('144',1,2,'Zaiross'),
('144',1,3,'Jamire'),
('144',1,4,'Zerath'),
('144',1,5,'Grogen'),
('145',0,1,'Phoenix'),
('145',0,2,'Phoenix'),
('145',0,3,'Phoenix'),
('145',0,4,'Phoenix'),
('145',0,5,'Phoenix'),
('145',1,1,'Sigmarus'),
('145',1,2,'Perna'),
('145',1,3,'Teshar'),
('145',1,4,'Eludia'),
('145',1,5,'Jaara'),
('146',0,1,'Chimera'),
('146',0,2,'Chimera'),
('146',0,3,'Chimera'),
('146',0,4,'Chimera'),
('146',0,5,'Chimera'),
('146',1,1,'Taor'),
('146',1,2,'Rakan'),
('146',1,3,'Lagmaron'),
('146',1,4,'Shan'),
('146',1,5,'Zeratu'),
('147',0,1,'Vampire'),
('147',0,2,'Vampire'),
('147',0,3,'Vampire'),
('147',0,4,'Vampire'),
('147',0,5,'Vampire'),
('147',1,1,'Liesel'),
('147',1,2,'Verdehile'),
('147',1,3,'Argen'),
('147',1,4,'Julianne'),
('147',1,5,'Cadiz'),
('148',0,1,'Viking'),
('148',0,2,'Viking'),
('148',0,3,'Viking'),
('148',0,4,'Viking'),
('148',0,5,'Viking'),
('148',1,1,'Huga'),
('148',1,2,'Geoffrey'),
('148',1,3,'Walter'),
('148',1,4,'Jansson'),
('148',1,5,'Janssen'),
('149',0,1,'Amazon'),
('149',0,2,'Amazon'),
('149',0,3,'Amazon'),
('149',0,4,'Amazon'),
('149',0,5,'Amazon'),
('149',1,1,'Ellin'),
('149',1,2,'Ceres'),
('149',1,3,'Hina'),
('149',1,4,'Lyn'),
('149',1,5,'Mara'),
('150',0,1,'Martial Cat'),
('150',0,2,'Martial Cat'),
('150',0,3,'Martial Cat'),
('150',0,4,'Martial Cat'),
('150',0,5,'Martial Cat'),
('150',1,1,'Mina'),
('150',1,2,'Mei'),
('150',1,3,'Naomi'),
('150',1,4,'Xiao Ling'),
('150',1,5,'Miho'),
('152',0,1,'Vagabond'),
('152',0,2,'Vagabond'),
('152',0,3,'Vagabond'),
('152',0,4,'Vagabond'),
('152',0,5,'Vagabond'),
('152',1,1,'Allen'),
('152',1,2,'Kai\'en'),
('152',1,3,'Roid'),
('152',1,4,'Darion'),
('152',1,5,'Jubelle'),
('153',0,1,'Epikion Priest'),
('153',0,2,'Epikion Priest'),
('153',0,3,'Epikion Priest'),
('153',0,4,'Epikion Priest'),
('153',0,5,'Epikion Priest'),
('153',1,1,'Rina'),
('153',1,2,'Chloe'),
('153',1,3,'Michelle'),
('153',1,4,'Iona'),
('153',1,5,'Rasheed'),
('154',0,1,'Magical Archer'),
('154',0,2,'Magical Archer'),
('154',0,3,'Magical Archer'),
('154',0,4,'Magical Archer'),
('154',0,5,'Magical Archer'),
('154',1,1,'Sharron'),
('154',1,2,'Cassandra'),
('154',1,3,'Ardella'),
('154',1,4,'Chris'),
('154',1,5,'Bethony'),
('155',0,1,'Rakshasa'),
('155',0,2,'Rakshasa'),
('155',0,3,'Rakshasa'),
('155',0,4,'Rakshasa'),
('155',0,5,'Rakshasa'),
('155',1,1,'Su'),
('155',1,2,'Hwa'),
('155',1,3,'Yen'),
('155',1,4,'Pang'),
('155',1,5,'Ran'),
('156',0,1,'Bounty Hunter'),
('156',0,2,'Bounty Hunter'),
('156',0,3,'Bounty Hunter'),
('156',0,4,'Bounty Hunter'),
('156',0,5,'Bounty Hunter'),
('156',1,1,'Wayne'),
('156',1,2,'Randy'),
('156',1,3,'Roger'),
('156',1,4,'Walkers'),
('156',1,5,'Jamie'),
('157',0,1,'Oracle'),
('157',0,2,'Oracle'),
('157',0,3,'Oracle'),
('157',0,4,'Oracle'),
('157',0,5,'Oracle'),
('157',1,1,'Praha'),
('157',1,2,'Juno'),
('157',1,3,'Seara'),
('157',1,4,'Laima'),
('157',1,5,'Giana'),
('158',0,1,'Imp Champion'),
('158',0,2,'Imp Champion'),
('158',0,3,'Imp Champion'),
('158',0,4,'Imp Champion'),
('158',0,5,'Imp Champion'),
('158',1,1,'Yaku'),
('158',1,2,'Fairo'),
('158',1,3,'Pigma'),
('158',1,4,'Shaffron'),
('158',1,5,'Loque'),
('159',0,1,'Mystic Witch'),
('159',0,2,'Mystic Witch'),
('159',0,3,'Mystic Witch'),
('159',0,4,'Mystic Witch'),
('159',0,5,'Mystic Witch'),
('159',1,1,'Megan'),
('159',1,2,'Rebecca'),
('159',1,3,'Silia'),
('159',1,4,'Linda'),
('159',1,5,'Gina'),
('160',0,1,'Grim Reaper'),
('160',0,2,'Grim Reaper'),
('160',0,3,'Grim Reaper'),
('160',0,4,'Grim Reaper'),
('160',0,5,'Grim Reaper'),
('160',1,1,'Hemos'),
('160',1,2,'Sath'),
('160',1,3,'Hiva'),
('160',1,4,'Prom'),
('160',1,5,'Thrain'),
('161',0,1,'Occult Girl'),
('161',0,2,'Occult Girl'),
('161',0,3,'Occult Girl'),
('161',0,4,'Occult Girl'),
('161',0,5,'Occult Girl'),
('161',1,1,'Anavel'),
('161',1,2,'Rica'),
('161',1,3,'Charlotte'),
('161',1,4,'Lora'),
('161',1,5,'Nicki'),
('162',0,1,'Death Knight'),
('162',0,2,'Death Knight'),
('162',0,3,'Death Knight'),
('162',0,4,'Death Knight'),
('162',0,5,'Death Knight'),
('162',1,1,'Fedora'),
('162',1,2,'Arnold'),
('162',1,3,'Briand'),
('162',1,4,'Conrad'),
('162',1,5,'Dias'),
('163',0,1,'Lich'),
('163',0,2,'Lich'),
('163',0,3,'Lich'),
('163',0,4,'Lich'),
('163',0,5,'Lich'),
('163',1,1,'Rigel'),
('163',1,2,'Antares'),
('163',1,3,'Fuco'),
('163',1,4,'Halphas'),
('163',1,5,'Grego'),
('164',0,1,'Skull Soldier'),
('164',0,2,'Skull Soldier'),
('164',0,3,'Skull Soldier'),
('164',0,4,'Skull Soldier'),
('164',0,5,'Skull Soldier'),
('165',0,1,'Living Armor'),
('165',0,2,'Living Armor'),
('165',0,3,'Living Armor'),
('165',0,4,'Living Armor'),
('165',0,5,'Living Armor'),
('165',1,1,'Nickel'),
('165',1,2,'Iron'),
('165',1,3,'Copper'),
('165',1,4,'Silver'),
('165',1,5,'Zinc'),
('166',0,1,'Dragon Knight'),
('166',0,2,'Dragon Knight'),
('166',0,3,'Dragon Knight'),
('166',0,4,'Dragon Knight'),
('166',0,5,'Dragon Knight'),
('166',1,1,'Chow'),
('166',1,2,'Laika'),
('166',1,3,'Leo'),
('166',1,4,'Jager'),
('166',1,5,'Ragdoll'),
('167',0,1,'Magical Archer Promo'),
('167',0,2,'Magical Archer Promo'),
('167',0,3,'Magical Archer Promo'),
('167',0,4,'Magical Archer Promo'),
('167',0,5,'Magical Archer Promo'),
('167',1,4,'Fami'),
('168',0,1,'Monkey King'),
('168',0,2,'Monkey King'),
('168',0,3,'Monkey King'),
('168',0,4,'Monkey King'),
('168',0,5,'Monkey King'),
('168',1,1,'Shi Hou'),
('168',1,2,'Mei Hou Wang'),
('168',1,3,'Xing Zhe'),
('168',1,4,'Qitian Dasheng'),
('168',1,5,'Son Zhang Lao'),
('169',0,1,'Samurai'),
('169',0,2,'Samurai'),
('169',0,3,'Samurai'),
('169',0,4,'Samurai'),
('169',0,5,'Samurai'),
('169',1,1,'Kaz'),
('169',1,2,'Jun'),
('169',1,3,'Kaito'),
('169',1,4,'Tosi'),
('169',1,5,'Sige'),
('170',0,1,'Archangel'),
('170',0,2,'Archangel'),
('170',0,3,'Archangel'),
('170',0,4,'Archangel'),
('170',0,5,'Archangel'),
('170',1,1,'Ariel'),
('170',1,2,'Velajuel'),
('170',1,3,'Eladriel'),
('170',1,4,'Artamiel'),
('170',1,5,'Fermion'),
('172',0,1,'Drunken Master'),
('172',0,2,'Drunken Master'),
('172',0,3,'Drunken Master'),
('172',0,4,'Drunken Master'),
('172',0,5,'Drunken Master'),
('172',1,1,'Mao'),
('172',1,2,'Xiao Chun'),
('172',1,3,'Huan'),
('172',1,4,'Tien Qin'),
('172',1,5,'Wei Shin'),
('173',0,1,'Kung Fu Girl'),
('173',0,2,'Kung Fu Girl'),
('173',0,3,'Kung Fu Girl'),
('173',0,4,'Kung Fu Girl'),
('173',0,5,'Kung Fu Girl'),
('173',1,1,'Xiao Lin'),
('173',1,2,'Hong Hua'),
('173',1,3,'Ling Ling'),
('173',1,4,'Liu Mei'),
('173',1,5,'Fei'),
('174',0,1,'Beast Monk'),
('174',0,2,'Beast Monk'),
('174',0,3,'Beast Monk'),
('174',0,4,'Beast Monk'),
('174',0,5,'Beast Monk'),
('174',1,1,'Chandra'),
('174',1,2,'Kumar'),
('174',1,3,'Ritesh'),
('174',1,4,'Shazam'),
('174',1,5,'Rahul'),
('175',0,1,'Mischievous Bat'),
('175',0,2,'Mischievous Bat'),
('175',0,3,'Mischievous Bat'),
('175',0,4,'Mischievous Bat'),
('175',0,5,'Mischievous Bat'),
('176',0,1,'Battle Scorpion'),
('176',0,2,'Battle Scorpion'),
('176',0,3,'Battle Scorpion'),
('176',0,4,'Battle Scorpion'),
('176',0,5,'Battle Scorpion'),
('177',0,1,'Minotauros'),
('177',0,2,'Minotauros'),
('177',0,3,'Minotauros'),
('177',0,4,'Minotauros'),
('177',0,5,'Minotauros'),
('177',1,1,'Urtau'),
('177',1,2,'Burentau'),
('177',1,3,'Eintau'),
('177',1,4,'Grotau'),
('177',1,5,'Kamatau'),
('178',0,1,'Lizardman'),
('178',0,2,'Lizardman'),
('178',0,3,'Lizardman'),
('178',0,4,'Lizardman'),
('178',0,5,'Lizardman'),
('178',1,1,'Kernodon'),
('178',1,2,'Igmanodon'),
('178',1,3,'Velfinodon'),
('178',1,4,'Glinodon'),
('178',1,5,'Devinodon'),
('179',0,1,'Hell Lady'),
('179',0,2,'Hell Lady'),
('179',0,3,'Hell Lady'),
('179',0,4,'Hell Lady'),
('179',0,5,'Hell Lady'),
('179',1,1,'Beth'),
('179',1,2,'Raki'),
('179',1,3,'Ethna'),
('179',1,4,'Asima'),
('179',1,5,'Craka'),
('180',0,1,'Brownie Magician'),
('180',0,2,'Brownie Magician'),
('180',0,3,'Brownie Magician'),
('180',0,4,'Brownie Magician'),
('180',0,5,'Brownie Magician'),
('180',1,1,'Orion'),
('180',1,2,'Draco'),
('180',1,3,'Aquila'),
('180',1,4,'Gemini'),
('180',1,5,'Korona'),
('181',0,1,'Kobold Bomber'),
('181',0,2,'Kobold Bomber'),
('181',0,3,'Kobold Bomber'),
('181',0,4,'Kobold Bomber'),
('181',0,5,'Kobold Bomber'),
('181',1,1,'Malaka'),
('181',1,2,'Zibrolta'),
('181',1,3,'Taurus'),
('181',1,4,'Dover'),
('181',1,5,'Bering'),
('182',0,1,'King Angelmon'),
('182',0,2,'King Angelmon'),
('182',0,3,'King Angelmon'),
('182',0,4,'King Angelmon'),
('182',0,5,'King Angelmon'),
('182',1,1,'Blue King Angelmon'),
('182',1,2,'Red King Angelmon'),
('182',1,3,'Gold King Angelmon'),
('182',1,4,'White King Angelmon'),
('182',1,5,'Dark King Angelmon'),
('183',0,1,'Sky Dancer'),
('183',0,2,'Sky Dancer'),
('183',0,3,'Sky Dancer'),
('183',0,4,'Sky Dancer'),
('183',0,5,'Sky Dancer'),
('183',1,1,'Mihyang'),
('183',1,2,'Hwahee'),
('183',1,3,'Chasun'),
('183',1,4,'Yeonhong'),
('183',1,5,'Wolyung'),
('184',0,1,'Taoist'),
('184',0,2,'Taoist'),
('184',0,3,'Taoist'),
('184',0,4,'Taoist'),
('184',0,5,'Taoist'),
('184',1,1,'Gildong'),
('184',1,2,'Gunpyeong'),
('184',1,3,'Woochi'),
('184',1,4,'Hwadam'),
('184',1,5,'Woonhak'),
('185',0,1,'Beast Hunter'),
('185',0,2,'Beast Hunter'),
('185',0,3,'Beast Hunter'),
('185',0,4,'Beast Hunter'),
('185',0,5,'Beast Hunter'),
('185',1,1,'Gangchun'),
('185',1,2,'Nangrim'),
('185',1,3,'Suri'),
('185',1,4,'Baekdu'),
('185',1,5,'Hannam'),
('186',0,1,'Pioneer'),
('186',0,2,'Pioneer'),
('186',0,3,'Pioneer'),
('186',0,4,'Pioneer'),
('186',0,5,'Pioneer'),
('186',1,1,'Woosa'),
('186',1,2,'Chiwu'),
('186',1,3,'Pungbaek'),
('186',1,4,'Nigong'),
('186',1,5,'Woonsa'),
('187',0,1,'Penguin Knight'),
('187',0,2,'Penguin Knight'),
('187',0,3,'Penguin Knight'),
('187',0,4,'Penguin Knight'),
('187',0,5,'Penguin Knight'),
('187',1,1,'Toma'),
('187',1,2,'Naki'),
('187',1,3,'Mav'),
('187',1,4,'Dona'),
('187',1,5,'Kuna'),
('188',0,1,'Barbaric King'),
('188',0,2,'Barbaric King'),
('188',0,3,'Barbaric King'),
('188',0,4,'Barbaric King'),
('188',0,5,'Barbaric King'),
('188',1,1,'Aegir'),
('188',1,2,'Surtr'),
('188',1,3,'Hraesvelg'),
('188',1,4,'Mimirr'),
('188',1,5,'Hrungnir'),
('189',0,1,'Polar Queen'),
('189',0,2,'Polar Queen'),
('189',0,3,'Polar Queen'),
('189',0,4,'Polar Queen'),
('189',0,5,'Polar Queen'),
('189',1,1,'Alicia'),
('189',1,2,'Brandia'),
('189',1,3,'Tiana'),
('189',1,4,'Elenoa'),
('189',1,5,'Lydia'),
('190',0,1,'Battle Mammoth'),
('190',0,2,'Battle Mammoth'),
('190',0,3,'Battle Mammoth'),
('190',0,4,'Battle Mammoth'),
('190',0,5,'Battle Mammoth'),
('190',1,1,'Talc'),
('190',1,2,'Granite'),
('190',1,3,'Olivine'),
('190',1,4,'Marble'),
('190',1,5,'Basalt'),
('191',0,1,'Fairy Queen'),
('191',0,2,'Fairy Queen'),
('191',0,3,'Fairy Queen'),
('191',0,4,'Fairy Queen'),
('191',0,5,'Fairy Queen'),
('191',1,4,'Fran'),
('192',0,1,'Ifrit'),
('192',0,2,'Ifrit'),
('192',0,3,'Ifrit'),
('192',0,4,'Ifrit'),
('192',0,5,'Ifrit'),
('192',1,1,'Theomars'),
('192',1,2,'Tesarion'),
('192',1,3,'Akhamamir'),
('192',1,4,'Elsharion'),
('192',1,5,'Veromos'),
('193',0,1,'Cow Girl'),
('193',0,2,'Cow Girl'),
('193',0,3,'Cow Girl'),
('193',0,4,'Cow Girl'),
('193',0,5,'Cow Girl'),
('193',1,1,'Sera'),
('193',1,2,'Anne'),
('193',1,3,'Hannah'),
('193',1,5,'Cassie'),
('194',0,1,'Pirate Captain'),
('194',0,2,'Pirate Captain'),
('194',0,3,'Pirate Captain'),
('194',0,4,'Pirate Captain'),
('194',0,5,'Pirate Captain'),
('194',1,1,'Galleon'),
('194',1,2,'Carrack'),
('194',1,3,'Barque'),
('194',1,4,'Brig'),
('194',1,5,'Frigate'),
('195',0,1,'Charger Shark'),
('195',0,2,'Charger Shark'),
('195',0,3,'Charger Shark'),
('195',0,4,'Charger Shark'),
('195',0,5,'Charger Shark'),
('195',1,1,'Aqcus'),
('195',1,2,'Ignicus'),
('195',1,3,'Zephicus'),
('195',1,4,'Rumicus'),
('195',1,5,'Calicus'),
('196',0,1,'Mermaid'),
('196',0,2,'Mermaid'),
('196',0,3,'Mermaid'),
('196',0,4,'Mermaid'),
('196',0,5,'Mermaid'),
('196',1,1,'Tetra'),
('196',1,2,'Platy'),
('196',1,3,'Cichlid'),
('196',1,4,'Molly'),
('196',1,5,'Betta'),
('197',0,1,'Sea Emperor'),
('197',0,2,'Sea Emperor'),
('197',0,3,'Sea Emperor'),
('197',0,4,'Sea Emperor'),
('197',0,5,'Sea Emperor'),
('197',1,1,'Poseidon'),
('197',1,2,'Okeanos'),
('197',1,3,'Triton'),
('197',1,4,'Pontos'),
('197',1,5,'Manannan'),
('198',0,1,'Magic Knight'),
('198',0,2,'Magic Knight'),
('198',0,3,'Magic Knight'),
('198',0,4,'Magic Knight'),
('198',0,5,'Magic Knight'),
('198',1,1,'Lapis'),
('198',1,2,'Astar'),
('198',1,3,'Lupinus'),
('198',1,4,'Iris'),
('198',1,5,'Lanett'),
('199',0,1,'Assassin'),
('199',0,2,'Assassin'),
('199',0,3,'Assassin'),
('199',0,4,'Assassin'),
('199',0,5,'Assassin'),
('199',1,1,'Stella'),
('199',1,2,'Lexy'),
('199',1,3,'Tanya'),
('199',1,4,'Natalie'),
('199',1,5,'Isabelle'),
('200',0,1,'Neostone Fighter'),
('200',0,2,'Neostone Fighter'),
('200',0,3,'Neostone Fighter'),
('200',0,4,'Neostone Fighter'),
('200',0,5,'Neostone Fighter'),
('200',1,1,'Ryan'),
('200',1,2,'Trevor'),
('200',1,3,'Logan'),
('200',1,4,'Lucas'),
('200',1,5,'Karl'),
('201',0,1,'Neostone Agent'),
('201',0,2,'Neostone Agent'),
('201',0,3,'Neostone Agent'),
('201',0,4,'Neostone Agent'),
('201',0,5,'Neostone Agent'),
('201',1,1,'Emma'),
('201',1,2,'Lisa'),
('201',1,3,'Olivia'),
('201',1,4,'Illiana'),
('201',1,5,'Sylvia'),
('202',0,1,'Martial Artist'),
('202',0,2,'Martial Artist'),
('202',0,3,'Martial Artist'),
('202',0,4,'Martial Artist'),
('202',0,5,'Martial Artist'),
('202',1,1,'Luan'),
('202',1,2,'Sin'),
('202',1,3,'Lo'),
('202',1,4,'Hiro'),
('202',1,5,'Jackie'),
('203',0,1,'Mummy'),
('203',0,2,'Mummy'),
('203',0,3,'Mummy'),
('203',0,4,'Mummy'),
('203',0,5,'Mummy'),
('203',1,1,'Nubia'),
('203',1,2,'Sonora'),
('203',1,3,'Namib'),
('203',1,4,'Sahara'),
('203',1,5,'Karakum'),
('204',0,1,'Anubis'),
('204',0,2,'Anubis'),
('204',0,3,'Anubis'),
('204',0,4,'Anubis'),
('204',0,5,'Anubis'),
('204',1,1,'Avaris'),
('204',1,2,'Khmun'),
('204',1,3,'Iunu'),
('204',1,4,'Amarna'),
('204',1,5,'Thebae'),
('205',0,1,'Desert Queen'),
('205',0,2,'Desert Queen'),
('205',0,3,'Desert Queen'),
('205',0,4,'Desert Queen'),
('205',0,5,'Desert Queen'),
('205',1,1,'Bastet'),
('205',1,2,'Sekhmet'),
('205',1,3,'Hathor'),
('205',1,4,'Isis'),
('205',1,5,'Nephthys'),
('206',0,1,'Horus'),
('206',0,2,'Horus'),
('206',0,3,'Horus'),
('206',0,4,'Horus'),
('206',0,5,'Horus'),
('206',1,1,'Qebehsenuef'),
('206',1,2,'Duamutef'),
('206',1,3,'Imesety'),
('206',1,4,'Wedjat'),
('206',1,5,'Amduat'),
('207',0,1,'Jack-o\'-lantern'),
('207',0,2,'Jack-o\'-lantern'),
('207',0,3,'Jack-o\'-lantern'),
('207',0,4,'Jack-o\'-lantern'),
('207',0,5,'Jack-o\'-lantern'),
('207',1,1,'Chilling'),
('207',1,2,'Smokey'),
('207',1,3,'Windy'),
('207',1,4,'Misty'),
('207',1,5,'Dusky'),
('208',0,1,'Frankenstein'),
('208',0,2,'Frankenstein'),
('208',0,3,'Frankenstein'),
('208',0,4,'Frankenstein'),
('208',0,5,'Frankenstein'),
('208',1,1,'Tractor'),
('208',1,2,'Bulldozer'),
('208',1,3,'Crane'),
('208',1,4,'Driller'),
('208',1,5,'Crawler'),
('209',0,1,'Elven Ranger'),
('209',0,2,'Elven Ranger'),
('209',0,3,'Elven Ranger'),
('209',0,4,'Elven Ranger'),
('209',0,5,'Elven Ranger'),
('209',1,1,'Eluin'),
('209',1,2,'Adrian'),
('209',1,3,'Erwin'),
('209',1,4,'Lucien'),
('209',1,5,'Isillen'),
('210',0,1,'Harg'),
('210',0,2,'Harg'),
('210',0,3,'Harg'),
('210',0,4,'Harg'),
('210',0,5,'Harg'),
('210',1,1,'Remy'),
('210',1,2,'Racuni'),
('210',1,3,'Raviti'),
('210',1,4,'Dova'),
('210',1,5,'Kroa'),
('211',0,1,'Fairy King'),
('211',0,2,'Fairy King'),
('211',0,3,'Fairy King'),
('211',0,4,'Fairy King'),
('211',0,5,'Fairy King'),
('211',1,1,'Psamathe'),
('211',1,2,'Daphnis'),
('211',1,3,'Ganymede'),
('211',1,4,'Oberon'),
('211',1,5,'Nyx'),
('212',0,1,'Panda Warrior'),
('212',0,2,'Panda Warrior'),
('212',0,3,'Panda Warrior'),
('212',0,4,'Panda Warrior'),
('212',0,5,'Panda Warrior'),
('212',1,1,'Mo Long'),
('212',1,2,'Xiong Fei'),
('212',1,3,'Feng Yan'),
('212',1,4,'Tian Lang'),
('212',1,5,'Mi Ying'),
('213',0,1,'Dice Magician'),
('213',0,2,'Dice Magician'),
('213',0,3,'Dice Magician'),
('213',0,4,'Dice Magician'),
('213',0,5,'Dice Magician'),
('213',1,1,'Reno'),
('213',1,2,'Ludo'),
('213',1,3,'Morris'),
('213',1,4,'Tablo'),
('213',1,5,'Monte'),
('214',0,1,'Harp Magician'),
('214',0,2,'Harp Magician'),
('214',0,3,'Harp Magician'),
('214',0,4,'Harp Magician'),
('214',0,5,'Harp Magician'),
('214',1,1,'Sonnet'),
('214',1,2,'Harmonia'),
('214',1,3,'Triana'),
('214',1,4,'Celia'),
('214',1,5,'Vivachel'),
('215',0,1,'Unicorn'),
('215',0,2,'Unicorn'),
('215',0,3,'Unicorn'),
('215',0,4,'Unicorn'),
('215',0,5,'Unicorn'),
('215',1,1,'Amelia'),
('215',1,2,'Helena'),
('215',1,3,'Diana'),
('215',1,4,'Eleanor'),
('215',1,5,'Alexandra'),
('216',0,1,'Unicorn (Human Form)'),
('216',0,2,'Unicorn (Human Form)'),
('216',0,3,'Unicorn (Human Form)'),
('216',0,4,'Unicorn (Human Form)'),
('216',0,5,'Unicorn (Human Form)'),
('216',1,1,'Amelia'),
('216',1,2,'Helena'),
('216',1,3,'Diana'),
('216',1,4,'Eleanor'),
('216',1,5,'Alexandra'),
('151',0,5,'Devilmon'),
('143',1,4,'Rainbowmon'),
('217',1,4,'Super Angelmon'),
('218',0,1,'Paladin'),
('218',0,2,'Paladin'),
('218',0,3,'Paladin'),
('218',0,4,'Paladin'),
('218',0,5,'Paladin'),
('218',1,1,'Josephine'),
('218',1,2,'Ophilia'),
('218',1,3,'Louise'),
('218',1,4,'Jeanne'),
('218',1,5,'Leona');
