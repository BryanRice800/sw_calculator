<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RuneAttributes Controller
 *
 * @property \App\Model\Table\RuneAttributesTable $RuneAttributes
 */
class RuneAttributesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Runes', 'Attributes']
        ];
        $runeAttributes = $this->paginate($this->RuneAttributes);

        $this->set(compact('runeAttributes'));
        $this->set('_serialize', ['runeAttributes']);
    }

    /**
     * View method
     *
     * @param string|null $id Rune Attribute id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $runeAttribute = $this->RuneAttributes->get($id, [
            'contain' => ['Runes', 'Attributes']
        ]);

        $this->set('runeAttribute', $runeAttribute);
        $this->set('_serialize', ['runeAttribute']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $runeAttribute = $this->RuneAttributes->newEntity();
        if ($this->request->is('post')) {
            $runeAttribute = $this->RuneAttributes->patchEntity($runeAttribute, $this->request->getData());
            if ($this->RuneAttributes->save($runeAttribute)) {
                $this->Flash->success(__('The rune attribute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rune attribute could not be saved. Please, try again.'));
        }
        $runes = $this->RuneAttributes->Runes->find('list', ['limit' => 200]);
        $attributes = $this->RuneAttributes->Attributes->find('list', ['limit' => 200]);
        $this->set(compact('runeAttribute', 'runes', 'attributes'));
        $this->set('_serialize', ['runeAttribute']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rune Attribute id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $runeAttribute = $this->RuneAttributes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $runeAttribute = $this->RuneAttributes->patchEntity($runeAttribute, $this->request->getData());
            if ($this->RuneAttributes->save($runeAttribute)) {
                $this->Flash->success(__('The rune attribute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rune attribute could not be saved. Please, try again.'));
        }
        $runes = $this->RuneAttributes->Runes->find('list', ['limit' => 200]);
        $attributes = $this->RuneAttributes->Attributes->find('list', ['limit' => 200]);
        $this->set(compact('runeAttribute', 'runes', 'attributes'));
        $this->set('_serialize', ['runeAttribute']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rune Attribute id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $runeAttribute = $this->RuneAttributes->get($id);
        if ($this->RuneAttributes->delete($runeAttribute)) {
            $this->Flash->success(__('The rune attribute has been deleted.'));
        } else {
            $this->Flash->error(__('The rune attribute could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
