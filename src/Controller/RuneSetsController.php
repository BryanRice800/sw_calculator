<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RuneSets Controller
 *
 * @property \App\Model\Table\RuneSetsTable $RuneSets
 */
class RuneSetsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Attributes']
        ];
        $runeSets = $this->paginate($this->RuneSets);

        $this->set(compact('runeSets'));
        $this->set('_serialize', ['runeSets']);
    }

    /**
     * View method
     *
     * @param string|null $id Rune Set id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $runeSet = $this->RuneSets->get($id, [
            'contain' => ['Attributes', 'Runes']
        ]);

        $this->set('runeSet', $runeSet);
        $this->set('_serialize', ['runeSet']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $runeSet = $this->RuneSets->newEntity();
        if ($this->request->is('post')) {
            $runeSet = $this->RuneSets->patchEntity($runeSet, $this->request->getData());
            if ($this->RuneSets->save($runeSet)) {
                $this->Flash->success(__('The rune set has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rune set could not be saved. Please, try again.'));
        }
        $attributes = $this->RuneSets->Attributes->find('list', ['limit' => 200]);
        $this->set(compact('runeSet', 'attributes'));
        $this->set('_serialize', ['runeSet']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rune Set id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $runeSet = $this->RuneSets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $runeSet = $this->RuneSets->patchEntity($runeSet, $this->request->getData());
            if ($this->RuneSets->save($runeSet)) {
                $this->Flash->success(__('The rune set has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rune set could not be saved. Please, try again.'));
        }
        $attributes = $this->RuneSets->Attributes->find('list', ['limit' => 200]);
        $this->set(compact('runeSet', 'attributes'));
        $this->set('_serialize', ['runeSet']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rune Set id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $runeSet = $this->RuneSets->get($id);
        if ($this->RuneSets->delete($runeSet)) {
            $this->Flash->success(__('The rune set has been deleted.'));
        } else {
            $this->Flash->error(__('The rune set could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
