<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;

/**
 * Runes Controller
 *
 * @property \App\Model\Table\RunesTable $Runes
 */
class RunesController extends AppController {

  public function initialize() {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  /**
   * Index method
   *
   * @return \Cake\Network\Response|null
   */
  public function index() {
    if ($this->request->is('ajax') || $_GET['ajax']) {
      $this->viewBuilder()->setLayout('ajax');
      $options = [
        'contain' => ['RuneAttributes', 'RuneSets'],
        'order' => ['Runes.rune_set_id' => 'DESC', 'Runes.slot_number' => 'ASC'],
        'search_data' => $this->request->getQuery(),
      ];
      $runesArray = $this->Runes->find('searchQuery', $options);
      if (isset($runesArray['errors'])) {
        $this->response->statusCode(412);
        $runes = $runesArray;
      }
      else {
        $runes = Hash::combine($runesArray, '{n}.id', '{n}');
      }
    }
    else {
      $this->paginate = [
        'contain' => ['RuneSets', 'RuneAttributes', 'Units', 'Units.UnitsList'],
        'conditions' => ['Runes.stars > ' => 0],
        'order' => ['unit_id' => 'DESC', 'Runes.slot_number' => 'ASC'],
      ];
      $runes = $this->paginate($this->Runes);
    }
    $this->set(compact('runes'));
    $this->set('_serialize', ['runes']);
  }

  /**
   * View method
   *
   * @param string|null $id Rune id.
   * @return \Cake\Network\Response|null
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function view($id = null) {
    $rune = $this->Runes->get($id, [
      'contain' => ['RuneSets', 'RuneAttributes', 'Units']
    ]);
    $this->set('rune', $rune);
    $this->set('_serialize', ['rune']);
  }

  /**
   * Add method
   *
   * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
   */
  public function add() {
    $rune = $this->Runes->newEntity();
    if ($this->request->is('post')) {
      $rune = $this->Runes->patchEntity($rune, $this->request->getData());
      if ($this->Runes->save($rune)) {
        $this->Flash->success(__('The rune has been saved.'));

        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('The rune could not be saved. Please, try again.'));
    }
    $runeSets = $this->Runes->RuneSets->find('list', ['limit' => 200]);
    $units = $this->Runes->Units->find('list', ['limit' => 200]);
    $this->set(compact('rune', 'runeSets', 'units'));
    $this->set('_serialize', ['rune']);
  }

  /**
   * Edit method
   *
   * @param string|null $id Rune id.
   * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null) {
    $rune = $this->Runes->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $rune = $this->Runes->patchEntity($rune, $this->request->getData());
      if ($this->Runes->save($rune)) {
        $this->Flash->success(__('The rune has been saved.'));
        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('The rune could not be saved. Please, try again.'));
    }
    $runeSets = $this->Runes->RuneSets->find('list', ['limit' => 200]);
    $units = $this->Runes->Units->find('list', ['limit' => 200]);
    $this->set(compact('rune', 'runeSets', 'units'));
    $this->set('_serialize', ['rune']);
  }

  /**
   * Delete method
   *
   * @param string|null $id Rune id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function delete($id = null) {
    $this->request->allowMethod(['post', 'delete']);
    $rune = $this->Runes->get($id);
    if ($this->Runes->delete($rune)) {
      $this->Flash->success(__('The rune has been deleted.'));
    }
    else {
      $this->Flash->error(__('The rune could not be deleted. Please, try again.'));
    }
    return $this->redirect(['action' => 'index']);
  }

  public function searchForm() {
    
  }

}
