<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;

/**
 * Units Controller
 *
 * @property \App\Model\Table\UnitsTable $Units
 */
class UnitsController extends AppController {

  /**
   * Index method
   *
   * @return \Cake\Network\Response|null
   */
  public function index() {
    $this->paginate = [
      'contain' => ['Elements', 'UnitsList'],
    ];
    $units = $this->paginate($this->Units);
    $this->set(compact('units'));
    $this->set('_serialize', ['units']);
  }

  /**
   * View method
   *
   * @param string|null $id Unit id.
   * @return \Cake\Network\Response|null
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function view($id = null) {
    $unit = $this->Units->get($id, [
      'contain' => ['Elements', 'UnitsList', 'Runes' => ['sort' => ['slot_number']], 'Runes.RuneSets', 'Runes.RuneAttributes', 'Runes.RuneAttributes.Attributes']
    ]);

    $unitList = $this->Units->UnitsList->find('all', ['conditions' => [
          'id' => $unit->units_list_id,
          'element_id' => $unit->element_id,
          'awaken' => 0
      ]])->first();

    $runeSetsArray = $this->Units->Runes->RuneSets->find('all', ['contain' => ['Attributes']])->toArray();
    $runeSets = Hash::combine($runeSetsArray, '{n}.id', '{n}');
    $runeSetsSelect = Hash::combine($runeSetsArray, '{n}.id', '{n}.description');
    $attributes = $this->Units->Runes->RuneAttributes->Attributes->find('list')->toArray();
    $this->set(compact('unit', 'unitList', 'runeSets', 'runeSetsSelect', 'attributes'));
  }

  /**
   * Add method
   *
   * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
   */
  public function add() {
    $this->response->statusCode(501);
    return;
//    $unit = $this->Units->newEntity();
//    if ($this->request->is('post')) {
//      $unit = $this->Units->patchEntity($unit, $this->request->getData());
//      if ($this->Units->save($unit)) {
//        $this->Flash->success(__('The unit has been saved.'));
//
//        return $this->redirect(['action' => 'index']);
//      }
//      $this->Flash->error(__('The unit could not be saved. Please, try again.'));
//    }
//    $elements = $this->Units->Elements->find('list', ['limit' => 200]);
//    $unitsList = $this->Units->UnitsList->find('list', ['limit' => 200]);
//    $users = $this->Units->Users->find('list', ['limit' => 200]);
//    $this->set(compact('unit', 'elements', 'unitsList', 'users'));
//    $this->set('_serialize', ['unit']);
  }

  /**
   * Edit method
   *
   * @param string|null $id Unit id.
   * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null) {
    $unit = $this->Units->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $unit = $this->Units->patchEntity($unit, $this->request->getData());
      if ($this->Units->save($unit)) {
        $this->Flash->success(__('The unit has been saved.'));

        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('The unit could not be saved. Please, try again.'));
    }
    $elements = $this->Units->Elements->find('list', ['limit' => 200]);
    $unitsList = $this->Units->UnitsList->find('list', ['limit' => 200]);
    $users = $this->Units->Users->find('list', ['limit' => 200]);
    $this->set(compact('unit', 'elements', 'unitsList', 'users'));
    $this->set('_serialize', ['unit']);
  }

  /**
   * Delete method
   *
   * @param string|null $id Unit id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function delete($id = null) {
    $this->request->allowMethod(['post', 'delete']);
    $unit = $this->Units->get($id);
    if ($this->Units->delete($unit)) {
      $this->Flash->success(__('The unit has been deleted.'));
    }
    else {
      $this->Flash->error(__('The unit could not be deleted. Please, try again.'));
    }

    return $this->redirect(['action' => 'index']);
  }

  /**
   * 
   */
  public function upload() {
    if (!$this->request->is('post')) {
      return;
    }
    $this->Units->createUnitsFromFile($this->request->getData('file'));
    $this->redirect('/units');
  }

}
