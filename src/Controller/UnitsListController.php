<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * UnitsList Controller
 *
 * @property \App\Model\Table\UnitsListTable $UnitsList
 */
class UnitsListController extends AppController {

  /**
   * Index method
   *
   * @return \Cake\Network\Response|null
   */
  public function index() {
    $this->paginate = [
      'contain' => ['Elements']
    ];
    $unitsList = $this->paginate($this->UnitsList);
    $this->set(compact('unitsList'));
    $this->set('_serialize', ['unitsList']);
  }

  /**
   * View method
   *
   * @param string|null $id Units List id.
   * @return \Cake\Network\Response|null
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function view($id = null) {
    $unitsList = $this->UnitsList->get($id, [
      'contain' => ['Elements']
    ]);

    $this->set('unitsList', $unitsList);
    $this->set('_serialize', ['unitsList']);
  }

  /**
   * Add method
   *
   * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
   */
  public function add() {
    $unitsList = $this->UnitsList->newEntity();
    if ($this->request->is('post')) {
      $unitsList = $this->UnitsList->patchEntity($unitsList, $this->request->getData());
      if ($this->UnitsList->save($unitsList)) {
        $this->Flash->success(__('The units list has been saved.'));

        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('The units list could not be saved. Please, try again.'));
    }
    $elements = $this->UnitsList->Elements->find('list', ['limit' => 200]);
    $this->set(compact('unitsList', 'elements'));
    $this->set('_serialize', ['unitsList']);
  }

  /**
   * Edit method
   *
   * @param string|null $id Units List id.
   * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null) {
    $unitsList = $this->UnitsList->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $unitsList = $this->UnitsList->patchEntity($unitsList, $this->request->getData());
      if ($this->UnitsList->save($unitsList)) {
        $this->Flash->success(__('The units list has been saved.'));

        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('The units list could not be saved. Please, try again.'));
    }
    $elements = $this->UnitsList->Elements->find('list', ['limit' => 200]);
    $this->set(compact('unitsList', 'elements'));
    $this->set('_serialize', ['unitsList']);
  }

  /**
   * Delete method
   *
   * @param string|null $id Units List id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function delete($id = null) {
    $this->request->allowMethod(['post', 'delete']);
    $unitsList = $this->UnitsList->get($id);
    if ($this->UnitsList->delete($unitsList)) {
      $this->Flash->success(__('The units list has been deleted.'));
    }
    else {
      $this->Flash->error(__('The units list could not be deleted. Please, try again.'));
    }

    return $this->redirect(['action' => 'index']);
  }

}
