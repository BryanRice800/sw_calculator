<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Behavior;

use Cake\ORM\Behavior;

class UploadFilesBehavior extends Behavior {

  var $config = [
    'target_dir' => WWW_ROOT . '/files',
    'allowed_mime_types' => ['application/json' => true, 'text/plain' => true]
  ];

  public function initialize(array $config) {
    parent::initialize($config);
    $this->config += $config;
    if (!is_dir($this->config['target_dir'])) {
      mkdir($this->config['target_dir']);
    }
  }

  public function uploadFile(array $file) {
    if (!$this->config['allowed_mime_types'][$file['type']]) {
      return;
    }
    $filename = $this->renameFile($file['name']);
    $finalName = $this->config['target_dir'] . '/' . $filename;
    if (move_uploaded_file($file['tmp_name'], $finalName)) {
      return $finalName;
    }
    return FALSE;
  }

  /**
   * 
   * @param type $filename
   */
  private function renameFile($filename) {
    $files = scandir($this->config['target_dir']);
    list($name, $ext) = $this->getOriginalFileName($filename);
    $count = 0;
    foreach ($files as $file) {
      if ($file === '.' || $file === '..') {
        continue;
      }
      if (preg_match("/$name/", $file)) {
        $count++;
      }
    }
    if ($count > 0) {
      return $name . '_' . ( ++$count) . '.' . $ext;
    }
    return $filename;
  }

  private function getOriginalFileName($filename) {
    $name = explode('.', $filename);
    $ext = array_pop($name);
    $nameOnly = implode('.', $name);
    $cleanName = explode('_', $nameOnly);
    if (count($cleanName) > 1) {
      array_pop($cleanName);
    }
    return [implode('_', $cleanName), $ext];
  }

}
