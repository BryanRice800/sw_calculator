<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rune Entity
 *
 * @property int $id
 * @property int $slot_number
 * @property int $stars
 * @property int $rune_set_id
 * @property int $unit_id
 *
 * @property \App\Model\Entity\RuneSet $rune_set
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\RuneAttribute[] $rune_attributes
 */
class Rune extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
