<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RuneAttribute Entity
 *
 * @property int $id
 * @property int $rune_id
 * @property int $delta
 * @property string $identifier
 * @property int $attribute_id
 * @property int $increase
 * @property bool $modified
 * @property int $boost
 *
 * @property \App\Model\Entity\Rune $rune
 * @property \App\Model\Entity\Attribute $attribute
 */
class RuneAttribute extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
