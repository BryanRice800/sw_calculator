<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Unit Entity
 *
 * @property int $id
 * @property int $element_id
 * @property int $hp
 * @property int $atk
 * @property int $def
 * @property int $spd
 * @property int $cri_rate
 * @property int $cri_damage
 * @property int $resistance
 * @property int $accuracy
 * @property bool $awaken
 * @property \Cake\I18n\Time $create_time
 * @property int $units_list_id
 * @property int $user_id
 *
 * @property \App\Model\Entity\Element $element
 * @property \App\Model\Entity\UnitsList $units_list
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Rune[] $runes
 */
class Unit extends Entity {

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
  ];
  protected $_virtual = ['unit_image'];

  protected function _getUnitImage() {
    $unit_name = $this->_properties['units_list']->description;
    $unit_is_awaken = $this->_properties['awaken'];
    $unit_element = (!empty($this->_properties['element']) && $this->_properties['element']->description ? $this->_properties['element']->description : '');
    $image = str_replace("'", "", preg_replace("/[\s_]/", "_", "{$unit_name}" . ($unit_is_awaken ? '' : "_$unit_element")));
    $uri = "img/monster_icons/" . strtolower($image) . ".png";
    $return = (file_exists($uri) ? $uri : "img/monster_icons/no_image.png");
    return $return;
  }

}
