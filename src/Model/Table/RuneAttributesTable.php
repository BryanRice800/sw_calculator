<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RuneAttributes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Runes
 * @property \Cake\ORM\Association\BelongsTo $Attributes
 *
 * @method \App\Model\Entity\RuneAttribute get($primaryKey, $options = [])
 * @method \App\Model\Entity\RuneAttribute newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RuneAttribute[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RuneAttribute|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RuneAttribute patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RuneAttribute[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RuneAttribute findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RuneAttributesTable extends Table {

  var $config = [
    'defaults' => [
      'rune_id' => '',
      'delta' => 0,
      'identifier' => '',
      'modified' => 0,
      'boost' => 0
    ],
    'labels' => [
      'pri_eff' => 'primary',
      'pref_eff' => 'prefix',
      'sec_eff_0' => 'first',
      'sec_eff_1' => 'second',
      'sec_eff_2' => 'third',
      'sec_eff_3' => 'fourth',
    ]
  ];

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config) {
    parent::initialize($config);

    $this->setTable('rune_attributes');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');

    $this->belongsTo('Runes', [
      'foreignKey' => 'rune_id',
      'joinType' => 'INNER'
    ]);
    $this->belongsTo('Attributes', [
      'foreignKey' => 'attribute_id',
      'joinType' => 'INNER'
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->integer('id')
      ->allowEmpty('id', 'create');

    $validator
      ->integer('delta')
      ->requirePresence('delta', 'create')
      ->notEmpty('delta');

    $validator
      ->allowEmpty('identifier');

    $validator
      ->integer('increase')
      ->requirePresence('increase', 'create')
      ->notEmpty('increase');

    $validator
      ->integer('boost')
      ->allowEmpty('boost');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
   * @return \Cake\ORM\RulesChecker
   */
  public function buildRules(RulesChecker $rules) {
    $rules->add($rules->existsIn(['rune_id'], 'Runes'));
    $rules->add($rules->existsIn(['attribute_id'], 'Attributes'));

    return $rules;
  }

  public function createAttributes($rune) {
    if (!isset($rune['rune_id'])) {
      return;
    }
    $attribute = $this->config['defaults'];
    $attribute['rune_id'] = $rune['rune_id'];
    foreach (['pri_eff' => 'pri_eff', 'pref_eff' => 'prefix_eff'] as  $staticEffects => $externalIdentifier) {
      $attribute['identifier'] = $staticEffects;
      $attribute['label'] = $this->config['labels'][$staticEffects];
      if (isset($rune[$externalIdentifier])) {
        $attribute['attribute_id'] = $rune[$externalIdentifier][0];
        $attribute['increase'] = $rune[$externalIdentifier][1];
      }
      else {
        $attribute['attribute_id'] = 0;
        $attribute['increase'] = 0;
      }
      $attributeEntity = $this->newEntity($attribute);
      $this->save($attributeEntity);
    }
    for ($i = 0; $i < 4; $i++) {
      $identifier = 'sec_eff_' . $i;
      $attribute['identifier'] = $identifier;
      $attribute['label'] = $this->config['labels'][$identifier];
      if (isset($rune['sec_eff'][$i])) {
        $attribute['attribute_id'] = $rune['sec_eff'][$i][0];
        $attribute['increase'] = $rune['sec_eff'][$i][1];
        $attribute['modified'] = $rune['sec_eff'][$i][2];
        $attribute['boost'] = $rune['sec_eff'][$i][3];
      }
      else {
        $attribute['attribute_id'] = 0;
        $attribute['increase'] = 0;
        $attribute['modified'] = 0;
        $attribute['boost'] = 0;
      }
      $attributeEntity = $this->newEntity($attribute);
      $this->save($attributeEntity);
    }
  }
}
