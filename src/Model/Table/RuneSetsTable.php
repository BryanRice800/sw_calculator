<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RuneSets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Attributes
 * @property \Cake\ORM\Association\HasMany $Runes
 *
 * @method \App\Model\Entity\RuneSet get($primaryKey, $options = [])
 * @method \App\Model\Entity\RuneSet newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RuneSet[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RuneSet|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RuneSet patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RuneSet[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RuneSet findOrCreate($search, callable $callback = null, $options = [])
 */
class RuneSetsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rune_sets');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->belongsTo('Attributes', [
            'foreignKey' => 'attribute_id'
        ]);
        $this->hasMany('Runes', [
            'foreignKey' => 'rune_set_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->integer('requirement')
            ->allowEmpty('requirement');

        $validator
            ->integer('increase_amount')
            ->allowEmpty('increase_amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['attribute_id'], 'Attributes'));

        return $rules;
    }
}
