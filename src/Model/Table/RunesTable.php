<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Runes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $RuneSets
 * @property \Cake\ORM\Association\BelongsTo $Units
 * @property \Cake\ORM\Association\HasMany $RuneAttributes
 *
 * @method \App\Model\Entity\Rune get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rune newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rune[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rune|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rune patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rune[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rune findOrCreate($search, callable $callback = null, $options = [])
 */
class RunesTable extends Table {

  /**
   * An array holding general configuration for runes
   * @var array <table><thead><tr><th>key</th><th>description</th></tr></thead><tbody><tr><td>mappings</td><td>an array holding the mappings between this model and incoming data form sw proxy.</td></tr></tobody></table>
   */
  var $config = [
    'mappings' => [
      'id' => 'rune_id',
      'slot_number' => 'slot_no',
      'stars' => 'class',
      'rune_set_id' => 'set_id',
      'unit_id' => 'unit_id',
      'upgrade_curr' => 'upgrade_curr',
    ],
    'search_options' => [
      'rune_set' => 1,
      'stars' => 1,
      'stars_or_higher' => 1,
      'level' => 1,
      'level_or_higher' => 1,
      'primary_effect' => 1,
      'min_value' => 1,
      'with_attributes' => 1,
      'slot_id' => 1,
    ]
  ];

  /**
   * @var int The id of a unit that this rune should be associated with.
   */
  var $unitId = null;

  /**
   *
   * @var array For consistency we need to ensure that every unit "has 6 runes", if a unit doesn't have 6 runes, a dummy value will be created for that slot.
   */
  var $slots = [1, 2, 3, 4, 5, 6];

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config, $unitId = null) {
    parent::initialize($config);

    $this->setTable('runes');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');

    $this->belongsTo('RuneSets', [
      'foreignKey' => 'rune_set_id',
      'joinType' => 'INNER'
    ]);
    $this->belongsTo('Units', [
      'foreignKey' => 'unit_id'
    ]);
    $this->hasMany('RuneAttributes', [
      'foreignKey' => 'rune_id'
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->allowEmpty('id', 'create');

    $validator
      ->integer('slot_number')
      ->requirePresence('slot_number', 'create')
      ->notEmpty('slot_number');

    $validator
      ->integer('stars')
      ->requirePresence('stars', 'create')
      ->notEmpty('stars');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
   * @return \Cake\ORM\RulesChecker
   */
  public function buildRules(RulesChecker $rules) {
    $rules->add($rules->existsIn(['rune_set_id'], 'RuneSets'));
    //$rules->add($rules->existsIn(['unit_id'], 'Units'));
    return $rules;
  }

  public function createFromUnit($runes, $unitId = null) {
    if (!is_array($runes)) {
      $runes = [];
    }
    $normalized = $this->normalizeUnitRunes($runes, $unitId);
    foreach ($normalized as $slot => $rune) {
      $data = $this->getRuneValues($rune, $unitId);
      $runeEntity = $this->getRuneEntity($data);
      if ($this->save($runeEntity)) {
        $this->RuneAttributes->createAttributes($rune);
      }
    }
  }

  public function createFromFile($data) {
    foreach ($data['runes'] as $rune) {
      //This rune belongs to a unit, therefore it will either be created by units or has already been created.
      if ($rune['occupied_id'] && $rune['occupied_id'] > 0) {
        continue;
      }
      $parsedData = $this->getRuneValues($rune);
      $runeEntity = $this->getRuneEntity($parsedData);
      if ($this->save($runeEntity)) {
        $this->RuneAttributes->createAttributes($rune);
      }
    }
  }

  /**
   * A patched entity with the new data if $data['id'] is found or a new entity from $data otherwise.
   * @param array $data the data to be found
   * @return \App\Model\Entity\Rune a rune entity ready to be saved.
   */
  private function getRuneEntity($data) {
    $exisitngRune = $this->find('all', ['conditions' => ['id' => $data['id']]])->first();
    if ($exisitngRune) {
      $runeEntity = $this->patchEntity($exisitngRune, $data);
      //Rune Attributes are "headless" data, its easier to just delete them and recreate them.
      $this->RuneAttributes->deleteAll(['rune_id' => $data['id']]);
    }
    else {
      $runeEntity = $this->newEntity($data);
    }
    return $runeEntity;
  }

  /**
   * Maps the values from sw proxy json file to our fields in the DB.
   * @param array $rune the rune array coming from sw parser.
   * @return array an array ready to be send to be patched as a rune entity.
   */
  public function getRuneValues($rune, $unitId = null) {
    $parsedRune = [];
    foreach ($this->config['mappings'] as $target => $source) {
      if ($source && isset($rune[$source])) {
        $parsedRune[$target] = $rune[$source];
      }
      else {
        $parsedRune[$target] = '0';
      }
    }
    $parsedRune['unit_id'] = !empty($parsedRune['unit_id']) ? $parsedRune['unit_id'] : $unitId;
    return $parsedRune;
  }

  /**
   * When creating runes for units, we need to force the system to create 6 runes for each unit.
   * @param array $runes an array representing the unit's runes.
   * @param int $unitId The unit's id that these runes will be normalized for.
   * @return array a normalized array containing strictly 6 elements representing a rune for each available slot.
   */
  private function normalizeUnitRunes(array $runes, $unitId) {
    $normalized = [];
    foreach ($runes as $rune) {
      if (isset($rune['slot_no'])) {
        $normalized[$rune['slot_no']] = $rune;
      }
    }
    foreach ($this->slots as $slot) {
      if (!isset($normalized[$slot])) {
        $normalized[$slot] = ['rune_id' => "{$unitId}{$slot}", 'slot_no' => $slot];
      }
    }
    ksort($normalized);
    return $normalized;
  }

  /**
   * Custom query finder for searching runes via search_runes form.
   * @param Cake\ORM\Query $query the query object send by the <i>find</i> method
   * @param array $options the options send by the <i>find</i> method, this array expects to have a 'search_data' key and the values must come from 'search_form'.
   * @return Cake\ORM\Query the modified query with custom conditions based on the data provided by search_data.
   */
  public function findSearchQuery($query, $options) {
    $seachData = $options['search_data'];
    $errors = $this->validateSearchInputs($seachData);
    $options['condition'] = isset($options['condition']) ? $options['condition'] : [];
    if (empty($errors)) {
      $this->buildQueryConditions($options, $seachData);
    }
    else {
      return ['errors' => $errors];
    }
    $query->where($options['condition']);
    $result = $query->toArray();
    if (!empty($seachData['primary_effect']) || !empty($seachData['with_attributes'])) {
      $result = $this->filterRuneByAttributes($result, $seachData);
    }
    return $result;
  }

  /**
   * Adds the condition statements based on $arguments
   * @param array $options the options array that will be injected with additional conditions.
   * @param array $arguments the conditions that will be used to modify the query.
   */
  private function buildQueryConditions(&$options, $arguments) {
    $options['condition']['slot_number'] = $arguments['slot_id'];
    if (!empty($arguments['rune_set'])) {
      $options['condition']['rune_set_id'] = $arguments['rune_set'];
    }
    if (!empty($arguments['stars'])) {
      $higher = !empty($arguments['stars_or_higher']) ? ' >= ' : '';
      $options['condition']['stars' . $higher] = $arguments['stars'];
    }
    if (!empty($arguments['level'])) {
      $higher = !empty($arguments['level_or_higher']) ? ' >= ' : '';
      $options['condition']['upgrade_curr' . $higher] = $arguments['level'];
    }
  }

  /**
   * Filters the fetched result from findSearchQuery to include only the runes matching the given attributes condtions.
   * @param array $result the array version of the query executed.
   * @param array $conditions the conditions array.
   * @retrun array the filtered array based on $conditions
   */
  private function filterRuneByAttributes($result, $conditions) {
    $return = [];
    foreach ($result as $result) {
      foreach ($result['rune_attributes'] as $attribute) {
        $primary_effect_condition = $attribute['identifier'] == 'pri_eff' && $attribute['attribute_id'] == $conditions['primary_effect'];
        $primary_effect_min_value = !empty($conditions['min_value']) ? $attribute['attribute_id'] >= $conditions['min_value'] : true;
        // Check  if the user requested for a primary effect filter
        if (($primary_effect_condition && $primary_effect_min_value)) {
          $return[] = $result;
          break;
        }
        //if the primary effect filter condition was not met and we dont have a request to match attributes, then exit the loop.
        $has_sub_attributes_query = !empty($conditions['with_attributes']);
        if (!$has_sub_attributes_query) {
          break;
        }
        //if the rune has a requested sub attribute, send it to the result.
        $has_sub_attribute = $attribute['identifier'] != 'pri_eff' && in_array($attribute['attribute_id'], $conditions['with_attributes']);
        if ($has_sub_attribute) {
          $return[] = $result;
          break;
        }
      }
    }
    return $return;
  }

  /**
   * Validates that the provided array from rune search has only valid inputs.
   * @param array $searchInputs the search data array.
   */
  private function validateSearchInputs(&$searchInputs) {
    //Remove extra arguments sent from requests
    foreach ($searchInputs as $key => $value) {
      if (!isset($this->config['search_options'][$key])) {
        unset($searchInputs[$key]);
      }
    }
    $validator = new Validator();
    $validator->requirePresence('slot_id');
    $validator->add('slot_id', 'validValue', [
      'rule' => ['range', 1, 6],
    ]);
    $validator->allowEmpty('rune_set');
    $validator->add('rune_set', 'validValue', [
      'rule' => ['range', 1]
    ]);
    $validator->allowEmpty('stars');
    $validator->add('stars', 'validValue', [
      'rule' => ['range', 1, 6]
    ]);
    $validator->allowEmpty('level');
    $validator->add('level', 'isInteger', [
      'rule' => ['range', 1, 15]
    ]);
    $validator->allowEmpty('primary_effect');
    $validator->add('primary_effect', 'validValue', [
      'rule' => ['range', 1]
    ]);
    $validator->allowEmpty('with_attributes');
    $validator->add('with_attributes', 'custom', [
      'rule' => [$this, 'validateRuneAttributes']
    ]);
    return $validator->errors($searchInputs);
  }

  /**
   * Custom validation rule for the "with_attributes" search param.
   * @param array $attributes an array of attributes that will be used for this search.
   * @param array $context the context sent by Validator class.
   * @return boolean true if $attributes is an array with integer values only, false otherwise.
   */
  public function validateRuneAttributes($attributes, $context) {
    if (!is_array($attributes)) {
      return false;
    }
    foreach ($attributes as $key => $value) {
      if (!(int) $value) {
        return false;
      }
    }
    return true;
  }

}
