<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UnitsList Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Elements
 *
 * @method \App\Model\Entity\UnitsList get($primaryKey, $options = [])
 * @method \App\Model\Entity\UnitsList newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UnitsList[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UnitsList|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UnitsList patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UnitsList[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UnitsList findOrCreate($search, callable $callback = null, $options = [])
 */
class UnitsListTable extends Table {

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config) {
    parent::initialize($config);

    $this->setTable('units_list');
    $this->setDisplayField('description');
    $this->setPrimaryKey(['id', 'awaken', 'element_id']);

    $this->belongsTo('Elements', [
      'foreignKey' => 'element_id',
      'joinType' => 'INNER'
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->integer('id')
      ->allowEmpty('id', 'create');

    $validator
      ->boolean('awaken')
      ->allowEmpty('awaken', 'create');

    $validator
      ->requirePresence('description', 'create')
      ->notEmpty('description');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
   * @return \Cake\ORM\RulesChecker
   */
  public function buildRules(RulesChecker $rules) {
    $rules->add($rules->existsIn(['element_id'], 'Elements'));

    return $rules;
  }
  
  public function beforeFind($event, Query $query, $options, $primary) {
    $query->order(['UnitsList.id' => 'asc', 'awaken' => 'asc', 'element_id' => 'asc']);
  }

}
