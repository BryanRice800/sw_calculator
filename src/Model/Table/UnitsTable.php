<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Units Model
 *
 */
class UnitsTable extends Table {

  var $config = [
    'mappings' => [
      'id' => 'unit_id',
      'element_id' => 'attribute',
      'unit_level' => 'unit_level',
      'hp' => '',
      'atk' => 'atk',
      'def' => 'def',
      'spd' => 'spd',
      'cri_rate' => 'critical_rate',
      'cri_damage' => 'critical_damage',
      'resistance' => 'resist',
      'accuracy' => 'accuracy',
      'create_time' => 'create_time',
      'awaken' => '',
      'units_list_id' => '',
      'stars' => 'class'
    ]
  ];

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config) {
    parent::initialize($config);

    $this->setTable('units');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');

    $this->belongsTo('Elements', [
      'foreignKey' => 'element_id',
      'joinType' => 'INNER'
    ]);
    $this->belongsTo('UnitsList', [
      'foreignKey' => ['units_list_id', 'awaken', 'element_id'],
      'joinType' => 'INNER'
    ]);
    $this->hasMany('Runes', [
      'foreignKey' => 'unit_id'
    ]);
    $this->addBehavior('UploadFiles');
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->requirePresence('id', 'create');

    $validator
      ->integer('hp')
      ->allowEmpty('hp');

    $validator
      ->integer('atk')
      ->allowEmpty('atk');

    $validator
      ->integer('def')
      ->allowEmpty('def');

    $validator
      ->integer('spd')
      ->allowEmpty('spd');

    $validator
      ->integer('cri_rate')
      ->allowEmpty('cri_rate');

    $validator
      ->integer('cri_damage')
      ->allowEmpty('cri_damage');

    $validator
      ->integer('resistance')
      ->allowEmpty('resistance');

    $validator
      ->integer('accuracy')
      ->allowEmpty('accuracy');

    $validator
      ->boolean('awaken')
      ->allowEmpty('awaken');

    $validator
      ->dateTime('create_time')
      ->allowEmpty('create_time');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
   * @return \Cake\ORM\RulesChecker
   */
  public function buildRules(RulesChecker $rules) {
    //$rules->add($rules->existsIn(['element_id'], 'Elements'));
    //$rules->add($rules->existsIn(['units_list_id'], 'UnitsList'));
    //$rules->add($rules->existsIn(['user_id'], 'Users'));

    return $rules;
  }

  public function createUnitsFromFile($file) {
    $filename = $this->uploadFile($file);
    $json = file_get_contents($filename);
    $data = json_decode($json, TRUE);
    $this->bulkCreateUnits($data);
    $this->Runes->createFromFile($data);
  }

  public function bulkCreateUnits($data) {
    if (!is_array($data['unit_list'])) {
      return;
    }
    foreach ($data['unit_list'] as $unit) {
      $parsedUnit = $this->getUnitValues($unit);
      $exisitngUnit = $this->find('all', ['conditions' => ['id' => $unit['unit_id']]])->first();
      if ($exisitngUnit) {
        $unitEntity = $this->patchEntity($exisitngUnit, $parsedUnit);
      }
      else {
        $unitEntity = $this->newEntity($parsedUnit);
      }
      if ($this->save($unitEntity) && !empty($unit['runes'])) {
        $this->Runes->createFromUnit($unit['runes'], $unitEntity->id);
      }
    }
  }

  public function getUnitValues($unit) {
    $parsedUnit = [];
    foreach ($this->config['mappings'] as $target => $source) {
      if ($source && $unit[$source]) {
        $parsedUnit[$target] = $unit[$source];
      }
    }
    $parsedUnit['hp'] = $unit['con'] * 15;
    $parsedUnit['units_list_id'] = (int) substr($unit['unit_master_id'], 0, 3);
    $parsedUnit['awaken'] = !!substr($unit['unit_master_id'], -2, 1);
//    $parsedUnit['create_time'] = strtotime($unit['create_time']);
    return $parsedUnit;
  }

  public function beforeFind($event, $query, $options, $primary) {
    $query->order(['stars' => 'DESC', 'unit_level' => 'DESC', 'Units.element_id' => 'ASC']);
  }

}
