<?php unset($attributes[13]); ?>
<div class="rune <?= $type ?> <?= $type == 'original' ? 'hide-element' : ' ' ?> col-xs-12">
    <?php foreach ($runes as $rune): ?>
      <div class="slot slot-<?= $rune->slot_number ?> col-xs-12 col-sm-6 col-md-4">
          <?= $this->element('Rune/toggle_search_form', ['rune' => $rune]) ?>
          <h4><?= __('Slot') ?> <?= $rune->slot_number ?></h4>
          <div class="rune-effect">
              <?= $this->Form->select('rune-set', $runeSetsSelect, ['value' => $rune->rune_set_id]) ?>
          </div>
          <table  cellpadding="0" cellspacing="0">
              <tr class="header">
                  <th class="text-center description" scope="col"><?= __('Attribute') ?></th>
                  <th class="text-center stat" scope="col"><?= __('Stat') ?></th>
                  <th class="text-center initial_value" scope="col"><?= __('Initial Value') ?></th>
                  <th class="text-center total" scope="col"><?= __('Total Increase') ?></th>
              </tr>

              <?php foreach ($rune->rune_attributes as $attribute): ?>
                <tr class="attribute <?= $attribute->identifier ?>">
                    <td class="description"><?= ucwords($attribute->label) ?></td>
                    <td class="stat"><?= $this->Form->select('', $attributes, ['value' => $attribute->attribute->id]) ?></td>
                    <td class="initial_value"><?= $this->Form->control('', ['value' => $attribute->increase + $attribute->boost]) ?></td>
                    <td class="total"></td>
                </tr>
              <?php endforeach; ?>
          </table>
      </div>
    <?php endforeach; ?>
</div>