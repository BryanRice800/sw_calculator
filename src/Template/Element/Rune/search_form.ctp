<?php unset($attributes[13]); ?>
<?= $this->Html->script('search_results.js', ['block' => 'script']) ?>
<?= $this->Html->script('search_form.js', ['block' => 'script']) ?>
<div class="search-form-wrapper col-xs-12 col-sm-6 hidden">
    <h1>
        <?= _('Search Runes') ?>
    </h1>
    <?= $this->Form->create(null, ['type' => 'get', 'url' => ['controller' => 'runes', 'action' => 'search'], 'id' => 'search-runes', 'name' => 'search-runes', 'class' => ['search-runes', 'col-xs-12']]) ?>
    <div class="rune-slot-info col-xs-12">
        <span><?= _('Rune Slot') ?>:</span> <span class="search-rune-slot"></span>
    </div>

    <fieldset class="search-rune-set col-xs-12">
        <legend><?= _('Rune Set') ?></legend>
        <?= $this->Form->control('rune_set', ['options' => $runeSets, 'class' => ['full-width']]) ?>
    </fieldset>
    <fieldset class="search-rune-stars-level col-xs-12">
        <legend><?= _('Stars and Level') ?></legend>
        <div class="search-rune-stars-level-wrapper">
            <?= $this->Form->control('stars', ['type' => 'integer', 'class' => ['']]) ?>
            <?= $this->Form->control('stars_or_higher', ['type' => 'checkbox', 'label' => _('Or Higher'), 'class' => ['']]) ?>
        </div>
        <div class="search-rune-stars-level-wrapper">
            <?= $this->Form->control('level', ['type' => 'integer', 'class' => ['']]) ?>
            <?= $this->Form->control('level_or_higher', ['type' => 'checkbox', 'label' => _('Or Higher'), 'class' => ['']]) ?>
        </div>
    </fieldset>
    <fieldset class="search-rune-attributes col-xs-12">
        <legend><?= _('Attributes') ?></legend>
        <?= $this->Form->control('primary_effect', ['type' => 'select', 'options' => $attributes, 'class' => ['col-xs-12']]) ?>
        <?= $this->Form->control('min_value', ['class' => ['col-xs-12']]) ?>
        <?= $this->Form->control('with_attributes', ['type' => 'select', 'multiple' => true, 'options' => $attributes, 'class' => ['col-xs-12']]) ?>
    </fieldset>
    <?= $this->Form->control('slot_id', ['type' => 'hidden']) ?>
    <fieldset class="wrapper buttons">
        <?= $this->Form->control('search', ['type' => 'submit', 'label' => false]) ?>
        <?= $this->Form->control('cancel', ['type' => 'button', 'label' => false]) ?>
    </fieldset>
</div>
<div class="col-xs-12 col-sm-6 search-runes-results hidden"></div>
