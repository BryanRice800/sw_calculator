<div class="unit <?= $type ?> col-xs-12 col-sm-6 text-center">
    <table class="stats vertical-table col-xs-12">
        <tr class="header">
            <th scope="col" class="header"><?= __('Stat') ?></th>
            <th scope="col" class="value"><?= __('Initial') ?></th>
            <th scope="col" class="boost"><?= __('Boost') ?></th>
            <th scope="col" class="total"><?= __('Total') ?></th>
        </tr>
        <tr class="unit-stat hp">
            <th scope="row" class="description"><?= __('Hp') ?></th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->hp]) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
        <tr class="unit-stat atk">
            <th scope="row" class="description"><?= __('Atk') ?></th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->atk]) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
        <tr class="unit-stat def">
            <th scope="row" class="description"><?= __('Def') ?></th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->def]) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
        <tr class="unit-stat spd">
            <th scope="row" class="description"><?= __('Spd') ?></th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->spd]) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
        <tr class="unit-stat cri_rate">
            <th scope="row" class="description"><?= __('Cri Rate') ?> %</th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->cri_rate, 'class' => 'percent']) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
        <tr class="unit-stat cri_damage">
            <th scope="row" class="description"><?= __('Cri Damage') ?> %</th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->cri_damage, 'class' => 'percent']) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
        <tr class="unit-stat resistance">
            <th scope="row" class="description"><?= __('Resistance') ?> %</th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->resistance, 'class' => 'percent']) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
        <tr class="unit-stat accuracy">
            <th scope="row" class="description"><?= __('Accuracy') ?> %</th>
            <td class="value"><?= $this->Form->control('', ['value' => $unit->accuracy, 'class' => 'percent']) ?></td>
            <td class="boost"></td>
            <td class="total"></td>
        </tr>
    </table>
</div>