<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="col-lg-2 col-sm-4 hidden-xs" id="actions-sidebar">
    <ul class="navbar navbar-left">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Units'), ['controller' => 'Units', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Upload Units'), ['controller' => 'Units', 'action' => 'upload']) ?></li>
    </ul>
</nav>
<div class="runes index col-lg-10 col-sm-8 col-xs-12 table-responsive">
    <h3><?= __('Runes') ?></h3>
    <table class="table table-hover table-responsive" cellpadding="0" cellspacing="0">
        <thead class="table-inverse">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('slot_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stars') ?></th>
                <th scope="col"><?= $this->Paginator->sort('level') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rune_set_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('unit_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($runes as $rune): ?>
              <tr class="<?= $rune->slot_number ?> <?= $rune->rune_set->description ?>">
                  <td><?= $this->Number->format($rune->slot_number) ?></td>
                  <td><?= $this->Number->format($rune->stars) ?></td>
                  <td><?= $this->Number->format($rune->upgrade_curr) ?></td>
                  <td><?= $rune->rune_set->description ?></td>
                  <td><?= $rune->has('unit') ? $this->Html->link($rune->unit->units_list->description, ['controller' => 'Units', 'action' => 'view', $rune->unit->id]) : 'no unit' ?></td>
                  <td class="actions">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $rune->id]) ?>
                  </td>
              </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
