<nav class="col-lg-2 col-sm-4 hidden-xs" id="actions-sidebar">
    <ul class="navbar navbar-left">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Units'), ['controller' => 'Units', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Upload Units'), ['controller' => 'Units', 'action' => 'upload']) ?></li>
    </ul>
</nav>
<div class="unitsList index col-lg-10 col-sm-8 col-xs-12 table-responsive">

</div>
