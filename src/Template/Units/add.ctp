<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="col-lg-2 col-sm-4 hidden-xs" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Units'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Elements'), ['controller' => 'Elements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Element'), ['controller' => 'Elements', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Units List'), ['controller' => 'UnitsList', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Units List'), ['controller' => 'UnitsList', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Runes'), ['controller' => 'Runes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rune'), ['controller' => 'Runes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="units form col-lg-10 col-sm-8 col-xs-12">
    <?= $this->Form->create($unit) ?>
    <fieldset>
        <legend><?= __('Add Unit') ?></legend>
        <?php
        echo $this->Form->control('element_id', ['options' => $elements]);
        echo $this->Form->control('hp');
        echo $this->Form->control('atk');
        echo $this->Form->control('def');
        echo $this->Form->control('spd');
        echo $this->Form->control('cri_rate');
        echo $this->Form->control('cri_damage');
        echo $this->Form->control('resistance');
        echo $this->Form->control('accuracy');
        echo $this->Form->control('awaken');
        echo $this->Form->control('create_time', ['empty' => true]);
        echo $this->Form->control('units_list_id', ['options' => $unitsList]);
        echo $this->Form->control('user_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
