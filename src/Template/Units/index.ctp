<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="col-sm-2 hidden-xs" id="actions-sidebar">
    <ul class="navbar navbar-left">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Units List'), ['controller' => 'UnitsList', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Units Upload'), ['controller' => 'Units', 'action' => 'upload']) ?></li>
    </ul>
</nav>
<div class="units index col-sm-10 col-xs-12 table-responsive">
    <h3><?= __('Units') ?></h3>
    <table class="table table-hover table-responsive" cellpadding="0" cellspacing="0">
        <thead class="table-inverse">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('units_list_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('unit_level') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stars') ?></th>
                <th scope="col"><?= $this->Paginator->sort('element_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('hp') ?></th>
                <th scope="col"><?= $this->Paginator->sort('atk') ?></th>
                <th scope="col"><?= $this->Paginator->sort('def') ?></th>
                <th scope="col"><?= $this->Paginator->sort('spd') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cri_rate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cri_damage') ?></th>
                <th scope="col"><?= $this->Paginator->sort('resistance') ?></th>
                <th scope="col"><?= $this->Paginator->sort('accuracy') ?></th>
                <th scope="col"><?= $this->Paginator->sort('awaken') ?></th>
                <th scope="col"><?= $this->Paginator->sort('create_time') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($units as $unit): ?>
              <tr>
                  <td>
                      <span class="unit_image"><img width="100" height="100" alt="<?= $unit->units_list->description ?>" src="<?= $unit->unit_image;  ?>"/></span>
                      <span class="unit_name"><?= $unit->has('units_list') ? $this->html->link($unit->units_list->description, ['controller' => 'units', 'action' => 'view', $unit->id]) : '' ?></span>
                  </td>
                  <td><?= $unit->unit_level ?></td>
                  <td><?= str_repeat('*', $unit->stars) ?></td>
                  <td><?= $unit->has('element') ? $unit->element->description : '' ?></td>
                  <td><?= $this->Number->format($unit->hp) ?></td>
                  <td><?= $this->Number->format($unit->atk) ?></td>
                  <td><?= $this->Number->format($unit->def) ?></td>
                  <td><?= $this->Number->format($unit->spd) ?></td>
                  <td><?= $this->Number->toPercentage($unit->cri_rate, 0) ?></td>
                  <td><?= $this->Number->toPercentage($unit->cri_damage, 0) ?></td>
                  <td><?= $this->Number->toPercentage($unit->resistance, 0) ?></td>
                  <td><?= $this->Number->toPercentage($unit->accuracy, 0) ?></td>
                  <td><?= h($unit->awaken ? __('Yes') : __('No')) ?></td>
                  <td><?= h($unit->create_time) ?></td>
              </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
