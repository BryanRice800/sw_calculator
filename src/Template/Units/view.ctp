<?php
/**
 * @var \App\View\AppView $this
 */
?>
<?= $this->assign('title', h($unit->units_list->description . ' (' . h($unit->element->description) . ' ' . $unitList->description) . ')') ?>
<?= $this->append('script') ?>
<?= $this->element('Jsdata/jsObject', ['name' => 'runeSets', 'array' => $runeSets]); ?>
<?= $this->element('Jsdata/jsObject', ['name' => 'attribues', 'array' => $attributes]); ?>
<?= $this->end() ?>
<?= $this->Html->script('unit_view.js', ['block' => 'script']) ?>
<?php $this->assign('title', h($unit->units_list->description . ' (' . h($unit->element->description) . ' ' . $unitList->description) . ')') ?>
<nav class="col-md-2 hidden-sm hidden-xs" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Units'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Upload Units'), ['controller' => 'Units', 'action' => 'upload']) ?> </li>
    </ul>
</nav>
<div class="units index col-md-10 col-xs-12">
    <?php foreach (['original', 'modified'] as $type): ?>
      <?= $this->element('Unit/unit', [$unit, 'type' => $type]) ?>
    <?php endforeach; ?>
    <div class="col-xs-12 power-ranking title text-center">
        <?= _('Power Rankings') ?>
    </div>
    <?php foreach (['original', 'modified'] as $type): ?>
      <div class="<?= $type ?> power-ranking col-xs-6">
          <span class="col-xs-6 ranking"><?= _('Attack') ?>: </span><span class="col-xs-6 attack-ranking">0</span>
          <span class="col-xs-6 ranking"><?= _('Speed') ?>: </span><span class="col-xs-6 speed-ranking">0</span>
          <span class="col-xs-6 ranking"><?= _('Health') ?>: </span><span class="col-xs-6 health-ranking">0</span>
          <span class="col-xs-6 ranking"><?= _('Total') ?>: </span><span class="col-xs-6 total-ranking">0</span>
      </div>
    <?php endforeach; ?>
    <?= $this->element('Rune/search_form', ['runeSets' => $runeSetsSelect]) ?>
    <div class="runes col-xs-12">
        <h4><?= __('Runes') ?></h4>
        <?php if (!empty($unit->runes)): ?>
          <?php foreach (['original', 'modified'] as $type): ?>
            <?= $this->element('Rune/rune', ['runes' => $unit->runes, 'type' => $type]) ?>
          <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
