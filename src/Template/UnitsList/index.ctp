<?php

/**
  * @var \App\View\AppView $this
  */
?>
<nav class="col-lg-2 col-sm-4 hidden-xs" id="actions-sidebar">
    <ul class="navbar navbar-left">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Upload Units'), ['controller' => 'Units', 'action' => 'upload']) ?></li>
        <li><?= $this->Html->link(__('Units'), ['controller' => 'Units', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="unitsList index col-lg-10 col-sm-8 col-xs-12 table-responsive">
    <h3><?= __('Units List') ?></h3>
    <table class="table table-hover table-responsive" cellpadding="0" cellspacing="0">
        <thead class="table-inverse">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('awaken') ?></th>
                <th scope="col"><?= $this->Paginator->sort('element_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($unitsList as $unitsList): ?>
            <tr>
                <td><?= $this->Number->format($unitsList->id) ?></td>
                <td><?= h($unitsList->awaken) ?></td>
                <td><?= $unitsList->has('element') ? $this->Html->link($unitsList->element->description, ['controller' => 'Elements', 'action' => 'view', $unitsList->element->id]) : '' ?></td>
                <td><?= h($unitsList->description) ?></td>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
