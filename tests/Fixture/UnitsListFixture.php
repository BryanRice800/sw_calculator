<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UnitsListFixture
 *
 */
class UnitsListFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'units_list';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'awaken' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'element_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'description' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => 'AKA name', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'fk_units_list_element' => ['type' => 'index', 'columns' => ['element_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'awaken', 'element_id'], 'length' => []],
            'units_list_ibfk_1' => ['type' => 'foreign', 'columns' => ['element_id'], 'references' => ['elements', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'awaken' => 1,
            'element_id' => 1,
            'description' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
