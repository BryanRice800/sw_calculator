<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RuneAttributesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RuneAttributesTable Test Case
 */
class RuneAttributesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RuneAttributesTable
     */
    public $RuneAttributes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rune_attributes',
        'app.runes',
        'app.attributes',
        'app.rune_sets'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RuneAttributes') ? [] : ['className' => 'App\Model\Table\RuneAttributesTable'];
        $this->RuneAttributes = TableRegistry::get('RuneAttributes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RuneAttributes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
