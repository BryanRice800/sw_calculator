<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RuneSetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RuneSetsTable Test Case
 */
class RuneSetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RuneSetsTable
     */
    public $RuneSets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rune_sets',
        'app.attributes',
        'app.rune_attributes',
        'app.runes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RuneSets') ? [] : ['className' => 'App\Model\Table\RuneSetsTable'];
        $this->RuneSets = TableRegistry::get('RuneSets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RuneSets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
