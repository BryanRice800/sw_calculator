<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RunesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RunesTable Test Case
 */
class RunesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RunesTable
     */
    public $Runes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.runes',
        'app.rune_sets',
        'app.attributes',
        'app.rune_attributes',
        'app.units',
        'app.elements',
        'app.units_list'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Runes') ? [] : ['className' => 'App\Model\Table\RunesTable'];
        $this->Runes = TableRegistry::get('Runes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Runes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
