<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UnitsListTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UnitsListTable Test Case
 */
class UnitsListTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UnitsListTable
     */
    public $UnitsList;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.units_list',
        'app.elements',
        'app.units',
        'app.runes',
        'app.rune_sets',
        'app.attributes',
        'app.rune_attributes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UnitsList') ? [] : ['className' => 'App\Model\Table\UnitsListTable'];
        $this->UnitsList = TableRegistry::get('UnitsList', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UnitsList);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
