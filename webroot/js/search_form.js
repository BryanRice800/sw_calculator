/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function loadSearchFormScript() {
    var form = document.forms.namedItem('search-runes');
    form.addEventListener('submit', function (ev) {
        ev.preventDefault();
        requestRunes();
    }, false);
}

function requestRunes() {
    var ajax = new XMLHttpRequest();
    ajax.onload = function (progressEvent) {
        if (ajax.status !== 200) {
            console.error('ajax.readyState', ajax.readyState, 'ajax.status', ajax.status);
            return;
        }
        buildSearchResults(progressEvent.currentTarget.responseText);
    }
    ajax.open('get', '/runes' + getFormData(), true);
    ajax.setRequestHeader('Accept', 'application/json');
    ajax.setRequestHeader('Content-Type', 'application/json');
    ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    ajax.send(null);
}

function getFormData() {
    var searchData = document.querySelectorAll('.search-runes input, .search-runes select');
    var values = [];
    for (var i = 0; i < searchData.length; i = i + 1) {
        var element = searchData[i];
        var name = element.name;
        if (!element.id || !name) {
            continue;
        }
        switch (element.type) {
            case 'checkbox':
                values.push(encodeURIComponent(name) + '=' + (element.checked ? 1 : 0));
                break;
            case 'select-multiple':
                for (var item of element.options) {
                    if (item.selected) {
                        values.push(encodeURIComponent(name) + '=' + item.value);
                    }
                }
                break;
            default:
                values.push(encodeURIComponent(name) + '=' + element.value);
                break;
        }

    }
    return '?' + values.join('&');
}

document.addEventListener("DOMContentLoaded", loadSearchFormScript);