/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
String.prototype.toPropperCase = function () {
    return this.split(' ').map(function (val) {
        return val.charAt(0).toUpperCase() + val.substr(1).toLowerCase();
    }).join(' ');
}

function buildSearchResults(response) {
    var results = response ? JSON.parse(response).runes : {};
    var container = document.querySelector('.search-runes-results');
    container.innerHTML = '';
    for (var id in results) {
        var rune = results[id];
        var node = document.createElement('div'), classes = document.createAttribute('class'), runeHeader = document.createElement('div'),
                runeHeaderText = document.createTextNode('Level: ' + rune.upgrade_curr + ', Stars: ' + '*'.repeat(rune.stars) + ' (' + rune.stars + ')');
        classes.value = ['col-xs-12', 'search-result', rune.rune_set.description, (rune.unit_id ? 'owned' : 'free'), ('stars-' + rune.stars)].join(' ');
        node.setAttributeNode(classes);
        node.setAttribute('id', id);
        runeHeader.appendChild(runeHeaderText);
        container.appendChild(runeHeader);
        container.appendChild(appendRuneEffects(node, rune));
    }
}

function appendRuneEffects(node, rune) {
    rune.rune_attributes.forEach(function (attribute, index, rune_attributes) {
        var container = document.createElement('div'), classes = document.createAttribute('class'), attrName = window.attribues[attribute.attribute_id];
        var totalIncrease = (attribute.increase + attribute.boost);
        var tampered = attribute.modified || attribute.boost;
        var text = attrName.split('_').join(' ').toPropperCase().replace('P', '%') + ': ' + totalIncrease;
        var _this = this;
        if (tampered) {
            text += '(' + attribute.increase + ' + ' + attribute.boost + ')';
        }
        var textNode = document.createTextNode(text);
        classes.value = [attribute.label.toLowerCase(), (tampered ? 'tempered' : ''), attrName.toLowerCase()].join(' ');
        container.setAttributeNode(classes);
        container.appendChild(textNode);
        container.dataset.property = attribute.identifier;
        container.dataset.increase = totalIncrease;
        container.addEventListener('click', function (e) {
            prepareRuneForImport(attribute.rune_id, e, _this);
        })
        this.appendChild(container);
    }, node);
    return node;
}

function prepareRuneForImport(id, event, row) {
    var selectedElements = row.parentElement.querySelectorAll('.selected');
    for (var i = 0; i < selectedElements.length; i = i + 1) {
        selectedElements[i].className = selectedElements[i].className.replace(/selected/ig, '').trim();
    }
    row.className += ' selected';
}