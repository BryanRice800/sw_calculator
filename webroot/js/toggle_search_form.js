/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function attachListeners() {
    var buttons = document.querySelectorAll('.toggle-search');
    for (var b = 0; b < buttons.length; b = b + 1) {
        buttons[b].addEventListener('click', showSearchForm.bind(buttons[b]));
    }
    var cancel = document.querySelector('.search-form-wrapper #cancel');
    cancel.addEventListener('click', hideSearchForm);
}

function showSearchForm(element) {
    var wrappers = document.querySelectorAll('.search-form-wrapper, .search-runes-results');
    var slot_id = document.querySelector('.search-form-wrapper form #slot-id');
    var searcfFor = document.querySelector('.search-form-wrapper .search-rune-slot');
    var len = wrappers.length;
    for (var l = 0; l < len; l = l + 1) {
        wrappers[l].className = wrappers[l].className.replace(/hidden/ig, '').trim();
    }
    slot_id.value = element.srcElement.dataset.slot;
    searcfFor.innerHTML = element.srcElement.dataset.slot;
}

function hideSearchForm(element) {
    var wrapper = document.querySelectorAll('.search-form-wrapper, .search-runes-results');
    var slot_id = document.querySelector('.search-form-wrapper form #slot-id');
    for (var i = 0; i < wrapper.length; i = i + 1) {
        var hidden = wrapper[i].className.match(/hidden/i);
        if (!hidden) {
            wrapper[i].className = wrapper[i].className.trim() + ' hidden';
        }
    }
    slot_id.value = false;
}

document.addEventListener("DOMContentLoaded", attachListeners);
