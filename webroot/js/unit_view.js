/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var unitStats = {};
var unitBoosts = {};
var runeEffects = {};
function start() {
    getStats();
    var elements = document.querySelectorAll('.unit input, .rune input, .rune select');
    var len = elements.length;
    var events = {
        'text': 'input',
        'select-one': 'change'
    };
    for (var i = 0; i < len; i = i + 1) {
        elements[i].addEventListener('change', function (e) {
            getStats(['modified', 'original']);
        }, false);
    }
}

/**
 * Wrapper function that calcualtes all the attributes increased stats and the unts total stats boosts.
 * @param {string} states modified|original
 * @returns {undefined}
 */
function getStats(states) {
    states = states || ['original', 'modified'];
    for (var v in states) {
        getUnitStats(states[v]);
        getRuneAttributesBoosts(states[v]);
        getRuneEffectsBoosts(states[v]);
        displayUnitStatsBoost(states[v]);
        getPowerRanking(states[v]);
    }
}

/**
 * Gets the values from the unit's stats and stores them in global unitStats
 * @param {string} state modified|original
 * @returns {undefined}
 */
function getUnitStats(state) {
    var unitStatsDom = document.querySelectorAll('.unit.' + state + ' .unit-stat');
    var len = unitStatsDom.length;
    unitStats[state] = {};
    for (var index = 0; index < len; index = index + 1) {
        var classes = unitStatsDom[index].className.split(' ');
        var value = unitStatsDom[index].getElementsByTagName('input');
        unitStats[state][classes[1]] = value[0].value;
    }
}

/**
 * Gets all the boosts from all of the rune's attributes and stors them in global var unitBoosts;
 * @param {string} state modified|original
 * @returns {undefined}
 */
function getRuneAttributesBoosts(state) {
    var runeAttributeBoostsDom = document.querySelectorAll('.rune.' + state + ' .attribute');
    var len = runeAttributeBoostsDom.length;
    unitBoosts[state] = {};
    for (var index = 0; index < len; index = index + 1) {
        var stat = runeAttributeBoostsDom[index].getElementsByTagName('select');
        var attribute = stat[0].options[stat[0].value].text.toLowerCase();
        var value = runeAttributeBoostsDom[index].getElementsByTagName('input');
        if (unitBoosts[state].hasOwnProperty(attribute)) {
            unitBoosts[state][attribute] += parseInt(value[0].value);
        } else {
            unitBoosts[state][attribute] = parseInt(value[0].value);
        }
        displayAttributeBoost(state, attribute, parseInt(value[0].value), runeAttributeBoostsDom[index]);
    }
}

/**
 * Displays the total boost gained from the given attribute
 * @param {string} state modified|original
 * @param {string} attribute one of the attributes being added
 * @param {float} value the amount that the attribut will be increased
 * @param {DOMElement} element the current table row being updated
 * @returns {undefined}
 */
function displayAttributeBoost(state, attribute, value, element) {
    var boost = getBoost(state, attribute, value)['attributeBoost'];
    var displayElement = element.getElementsByClassName('total');
    displayElement[0].innerHTML = (isNaN(boost) ? 0 : Math.ceil(boost));
}

/**
 * If this is a percent attribute (ends with '_p'), it will add the attribute * amount / 100 otherwise just the amount will be returned.
 * @param {string} state modified|original
 * @param {string} attribute one of the attributes being added
 * @param {float} amount the amount that the attribut will be increased
 * @returns {object} an object with the real stat name (without the _p suffix) and the total stat boost
 */
function getBoost(state, attribute, amount) {
    var realAttribute = attribute.split('_p');
    realAttribute[0] = realAttribute[0].toLowerCase()
    var attributeBoost = 0;
    if (realAttribute.length > 1) {
        attributeBoost = (parseFloat(amount) / 100) * parseFloat(unitStats[state][realAttribute[0]]);
    } else {
        attributeBoost = parseFloat(amount);
    }
    return {'realAttribute': realAttribute[0], 'attributeBoost': attributeBoost};
}

/**
 * Gets the toal boost per stat from all runes.
 * @param {string} state modified|original
 * @returns {undefined}
 */
function getRuneEffectsBoosts(state) {
    var runeBoostsDom = document.querySelectorAll('.rune.' + state + ' .rune-effect');
    var len = runeBoostsDom.length;
    unitStats[state] = unitStats[state] || {};
    runeEffects[state] = {};
    for (var index = 0; index < len; index = index + 1) {
        var runeEffect = runeBoostsDom[index].getElementsByTagName('select');
        if (runeEffects[state].hasOwnProperty(runeEffect[0].value)) {
            runeEffects[state][runeEffect[0].value] += 1;
        } else {
            runeEffects[state][runeEffect[0].value] = 1;
        }
    }
    for (var v in runeEffects[state]) {
        if (runeSets.hasOwnProperty(v)) {
            var count = Math.floor(parseFloat(runeEffects[state][v]) / runeSets[v].requirement);
            var boost = count * parseFloat(runeSets[v].increase_amount);
            var attribute = runeSets[v].attribute.description.toLowerCase();
            if (unitBoosts[state].hasOwnProperty(attribute)) {
                unitBoosts[state][attribute] += boost;
            } else {
                unitBoosts[state][attribute] = boost;
            }
        }
    }
}

/**
 * Displays the unit stats increased attributes from the runes, it will render the boost gained as well as the total value for each stat.
 * @param {string} state modified|original
 * @returns {undefined}
 */
function displayUnitStatsBoost(state) {
    var unitStatsDom = document.querySelectorAll('.unit.' + state + ' .unit-stat');
    var len = unitStatsDom.length;
    var totalBoosts = {};
    for (var v in unitBoosts[state]) {
        var data = getBoost(state, v, unitBoosts[state][v]);
        if (totalBoosts.hasOwnProperty(data.realAttribute)) {
            totalBoosts[data.realAttribute] += data.attributeBoost;
        } else {
            totalBoosts[data.realAttribute] = parseFloat(data.attributeBoost);
        }
    }
    unitBoosts[state] = totalBoosts;
    for (var index = 0; index < len; index = index + 1) {
        var classes = unitStatsDom[index].className.split(' ');
        var boost = totalBoosts[classes[1]] ? Math.ceil(totalBoosts[classes[1]]) : 0;
        var total = parseFloat(unitStats[state][classes[1]]);
        unitStatsDom[index].getElementsByClassName('boost')[0].innerHTML = boost;
        displayAttributeBoost(state, classes[1], boost + total, unitStatsDom[index]);
    }
}

function getPowerRanking(state) {
    var stats = {};
    for (var v in unitStats[state]) {
        var stat = unitStats[state][v] ? parseFloat(unitStats[state][v]) : 0;
        var boost = unitBoosts[state][v] ? parseFloat(unitBoosts[state][v]) : 0;
        stats[v] = stat + boost;
    }

    var atkRanking = (stats['atk'] + (stats['cri_rate'] * stats['cri_damage'])) * (1 + (stats['accuracy'] / 100));
    var spdRanking = Math.pow(stats['spd'], 0.5);
    var hpRanking = (stats['resistance'] / 100) * (stats['hp'] * (1 - (1000 / (1000 + (3 * stats['def'])))));
    var ranking = Math.round((atkRanking * spdRanking) + hpRanking) / 100;
    displayRankingInfo(state, Math.round(atkRanking), Math.round(spdRanking * 100) / 100, Math.round(hpRanking), ranking);
}

function displayRankingInfo(state, atkRanking, spdRanking, hpRanking, ranking) {
    var rankings = {'attack': atkRanking, 'speed': spdRanking, 'health': hpRanking, 'total': ranking};
    for (var v in rankings) {
        var rankingElement = document.querySelectorAll('.' + state + '.power-ranking span.' + v + '-ranking');
        rankingElement[0].innerHTML = rankings[v];
    }
}

//Main function, everything starts on content loaded in juery this is $(document).ready(function(){});
document.addEventListener("DOMContentLoaded", start);


